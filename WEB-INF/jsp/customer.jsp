<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="customer"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Customer Management</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Add Customer</a></h2>
			<form:form method="post" action="add.htm"  commandName="customer">
			<table>
				<tr>
					<td>Customer ID :</td>
					<td><b id="custId">New</b><form:hidden path="custId"/></td>
				</tr>
				<tr>
					<td>Customer Name :</td>
					<td><form:input path="custName" /><font color="red">*</font></td>
				
					<td>Customer Mobile :</td>
					<td><form:input path="custMobile" onkeyup="javascript:validatenumber(this);" maxlength="10"/></td>
				</tr>
				<tr>
					<td>Customer Address :</td>
					<td><textarea cols="30" rows="4" name="custAddr"></textarea></td>
				
					<td>Customer Email :</td>
					<td><form:input path="custEmail" onblur="javascript:checkEmail(this);" /></td>
				</tr>
				<tr>
					<td colspan="2"><input id="Add Customer" class="button" type="submit" value="Add Customer" onclick="javascript:checkCustomerAction(this.form);" /><input type="reset" class="button" value="Clear" onclick="javascript:doClearCustomer(this.form);" /></td>
				</tr>
			</table>
			</form:form>
			<c:if test="${error != null }"><li><font color="red">${error }</font></li></c:if>
		</div>
		<div class="post">
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">List of Customer</a></h2>
			<div class="entry">
				<form:form action="delete.htm" commandName="customer" id="formEdit">
			<c:if test="${fn:length(custList) > 0}">
				<table cellpadding="5">
					<tr>
						<th align="left" width="20"><input type="submit" class="button" value="Delete" /></th>
						<th>Name</th>
						<th>Mobile</th>
						<th>Email</th>
						<th>&nbsp;</th>		
					</tr>
					
					<c:forEach items="${custList}" var="customer" varStatus="status">
						<c:if test="${status.count % 2 == 0}">
						<tr>						
						</c:if>
						<c:if test="${status.count % 2 != 0}">
						<tr bgcolor="#C0C0C0" style="color: brown;">						
						</c:if>
							<td><input type="checkbox" name="selectedCust" value="${customer.custId}" /></td>
							<td title="${customer.custAddr}">${customer.custName}</td>
							<td>${customer.custMobile}</td>
							<td><a href="../email/email.htm?emailAddr=${customer.custEmail}">${customer.custEmail}</a></td>
							<td><input type="button" class="button" value="Update" onclick="javascript:fillCustomerFields('${customer.custId}','${customer.custName}','${customer.custAddr}', '${customer.custMobile}', '${customer.custEmail}');" /></td>
						</tr>
					</c:forEach>
					
				</table>
			</c:if>
			</form:form>
			</div>
			<p class="meta"><a href="#" class="more">Maintain your returning customers here</a> </p>
		</div>
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>