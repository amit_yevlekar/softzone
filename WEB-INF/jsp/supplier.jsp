<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="supplier"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Supplier Management</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Add Supplier</a></h2>
			<form:form method="post" action="add.htm"  commandName="supplier">
			<table>
				<tr>
					<td>Supplier ID :</td>
					<td><b id="suppId">New</b><form:hidden path="suppId"/></td>
				</tr>
				<tr>
					<td>Supplier Name :</td>
					<td><form:input path="suppName" /></td>
				
					<td>Supplier Mobile :</td>
					<td><input id="suppMobileNo" type="text" name="suppMobileNo" maxlength="10" onkeyup="javascript:validatenumber(this);" /></td>
				</tr>
				<tr>
					<td>Supplier Code :</td>
					<td><form:input path="suppCode" /></td>
				
					<td>SoftZone Code :</td>
					<td><form:input path="softzoneCode" /></td>
				</tr>
				<tr>
					<td>Supplier Rate :</td>
					<td><form:input path="suppRate" /></td>
				
					<td>Supplier Email:</td>
					<td><form:input path="suppEmail" onblur="javascript:checkEmail(this);"/></td>
				</tr>
				
				<tr>
					<td><input id="Add Supplier" type="submit" class="button" value="Add Supplier" onclick="javascript:checkSupplierAction(this.form);"><input type="reset" class="button" value="Clear" onclick="javascript:doClearSupplier(this.form);" /></td>
				</tr>	
			</table>
			</form:form>
		</div>
		<div class="post">
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">List of Suppliers</a></h2>
			<div class="entry">
				<form:form action="delete.htm" commandName="supplier">
					<c:if test="${fn:length(supplierList) > 0}">
					<table cellpadding="5">					
						<tr>
							<th align="left" width="20"><input type="submit" class="button" value="Delete" /></th>
							<th>Supplier Name</th>
							<th>Supplier Code</th>
							<th>SoftZone Code</th>
							<th>Supplier Rate</th>
							<th>Mobile</th>
							<th>Email</th>
						</tr>
						
						<c:forEach items="${supplierList}" var="supplier" varStatus="status">
							
							<c:if test="${status.count % 2 == 0}">
							<tr>
							</c:if>
							<c:if test="${status.count % 2 != 0}">
							<tr bgcolor="#87421F">
							</c:if>
								<td><input type="checkbox" name="selectedSupplier" value="${supplier.suppId}" /></td>
								<td title="${supplier.suppEmail}"> ${supplier.suppName} </td>
								<td> ${supplier.suppCode} </td>
								<td> ${supplier.softzoneCode} </td>
								<td> ${supplier.suppRate} </td>
								<td> ${supplier.suppMobileNo} </td>
								<td><a href="../email/email.htm?emailAddr=${supplier.suppEmail}">${supplier.suppEmail} </a></td>
								<td><input type="button" class="button" value="Update" onclick="javascript:fillSupplierFields('${supplier.suppId}', '${supplier.suppName}', '${supplier.suppCode}', '${supplier.suppRate}', '${supplier.suppMobileNo}', '${supplier.suppEmail}', '${supplier.softzoneCode}');" /></td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
				</form:form>
			</div>
			<p class="meta"><a href="#" class="more">Supplier details are only available to admin users</a> </p>
		</div>
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>