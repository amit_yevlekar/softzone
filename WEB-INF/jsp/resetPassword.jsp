<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="login"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Reset Password</h1>
			<h2 class="title"><img src="../images/img05.gif"/>Login</h2>
			<form:form method="post" action="resetPwd.htm"  commandName="login">
				<table cellpadding="0" cellspacing="2">
					<tr>
						<td>Username :</td>
						<td><form:input path="username" readonly="true"/></td>						
					</tr>
					<tr>
						<td>Old Password :</td>
						<td><form:password path="oldPassword" /></td>						
					</tr>	
					<tr>
						<td>New Password :</td>
						<td><form:password path="password" /></td>
					</tr>
					
					<tr>
						<td colspan="2"><input type="submit" class="css3button" value="Reset Password" onclick="javascript:checkUserAction();" /></td>
					</tr>					
				</table>
				<c:if test="${error != null }"><li><font color="red">${error }</font></li></c:if>
			</form:form>
		</div>
		
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>