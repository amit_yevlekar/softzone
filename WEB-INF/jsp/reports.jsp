<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="report"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<script src="../js/highcharts.js"></script>
<script src="../js/exporting.js"></script>

<c:if test="${not empty report}">
<script type="text/javascript">
$(function () {
        $('#chart_div').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '${title}'
            },
            subtitle: {
                text: 'Softzone Inc.'
            },
            xAxis: {
                categories: [
                    ${category}
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: '${yAxisHeader}'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} ${metric}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [${jsData}]
        });
    });
    

		</script>

</c:if>
<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">			
			<h1 class="title">Report Management</h1>			
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Reports</a></h2>
			<br>
			<table>
			<tr align="center">
				<td width="200"><a href="report.htm?reportType=Turn%20Over"><font size="5" face="Georgia"><c:if test="${reportType=='Turn Over' }">&gt;<b></c:if>Turn Over<c:if test="${reportType=='Turn Over' }"></b>&lt;</c:if></font></a></td>
				<td width="200"><a href="report.htm?reportType=Order%20Status"><font size="5" face="Georgia"><c:if test="${reportType=='Order Status' }">&gt;<b></c:if>Order Status<c:if test="${reportType=='Order Status' }"></b>&lt;</c:if></font></a></td>
				<td width="200"><a href="openReport.htm?reportType=Tailor%20Payment"><font size="5" face="Georgia"><c:if test="${reportType=='Tailor Payment' }">&gt;<b></c:if>Tailor Payment<c:if test="${reportType=='Tailor Payment' }"></b>&lt;</c:if></font></a></td>
			</tr>			
			</table>
			
			<c:if test="${not empty ordYrs}">
				<c:forEach items="${ordYrs}" var="year" >
					<a href="openReport.htm?reportType=${reportType}&year=${year}"><font size="5" face="Georgia"><c:if test="${yearSel==year }">&gt;<b></c:if>${year}<c:if test="${yearSel==year }"></b>&lt;</c:if></font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;					
				</c:forEach>
			</c:if>
								
			<br>			
			
			<div id="chart_div" style="width: 900px; height: 500px;"></div>
			
		</div>
		<p class="meta">Check your reports</p>
	</div>
	<!-- end content -->
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>