<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="user"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">User Management</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Add User</a></h2>
			<form:form method="post" action="add.htm"  commandName="user">
			<table>
				<tr>
					<td>Username :</td>
					<td><form:input path="username" /><font color="red">*</font></td>
				</tr>					
				<tr>
					<td>Name :</td>
					<td><form:input path="name" /><font color="red">*</font></td>
				</tr>	
				<tr>
					<td>Email :</td>
					<td><form:input path="email" onblur="javascript:checkEmail(this);" /><font color="red">*</font></td>
				</tr>					
				<tr>
					<td>Admin :</td>
					<td><input type="checkbox" name="isAdmin" title="Check this to make user as admin" /></td>
				</tr>
				<tr>
					<td colspan="2"><input id="Add User" class="button" type="submit" value="Add User" onclick="javascript:checkUserAction(this.form);" /><input type="reset" class="button" value="Clear" onclick="javascript:doClearUser(this.form);" /></td>
				</tr>
			</table>
			</form:form>
			<c:if test="${error != null }"><li><font color="red">${error }</font></li></c:if>
		</div>
		<div class="post">
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">List of Users</a></h2>
			<div class="entry">
				<form:form action="delete.htm" commandName="user" id="formEdit">
			<c:if test="${fn:length(userList) > 0}">
				<table cellpadding="5">
					<tr>
						<th align="left" width="20"><input type="submit" class="button" value="Delete" /></th>
						<th>Name</th>
						<th>Email</th>
						<th>&nbsp;</th>		
					</tr>
					
					<c:forEach items="${userList}" var="user" varStatus="status">
						<c:if test="${status.count % 2 == 0}">
						<tr>						
						</c:if>
						<c:if test="${status.count % 2 != 0}">
						<tr bgcolor="#C0C0C0" style="color: brown;">						
						</c:if>
							<td><input type="checkbox" name="selectedUser" value="${user.username}" /></td>
							<td>${user.name} <c:if test="${user.isAdmin}"><img height="15" width="15" alt="" src="../images/Admin.png"></c:if>${ecode}</td>
							<td>${user.email}</td>
							<td><input type="button" class="button" value="Update" onclick="javascript:fillUserFields('${user.username}','${user.name}','${user.email}', '${user.isAdmin}');" /></td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="2"><img height="15" width="15" alt="" src="../images/Admin.png"> - Admin User</td>
					</tr>
				</table>
			</c:if>
			</form:form>
			</div>
			<p class="meta"><a href="#" class="more">Maintain your local users here</a> </p>
		</div>
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>