<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="login"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Login Page</h1>
			<h2 class="title"><img src="../images/img05.gif"/>Login</h2>
			<c:set var="loc2list" value="${fn:split(location, ',')}"/>
			<form:form action="doLogin.htm" commandName="login">
			<table>
				<tr>
					<td>USER ID :</td>
					<td><input type="text" name="username" /></td>
				</tr>
				<tr>
					<td>PASSWORD :</td>
					<td><form:password path="password" /></td>
				</tr>
				<tr>
					<td>LOCATION :</td>
					<td><select name="location" >
					<c:forEach items="${loc2list}" var="loc">
					<option value="${loc}">${loc}</option>
					</c:forEach>
					</select></td>
				</tr>				
				<tr>
					<td colspan="2"><input type="submit" class="button" value="Login"/>  <a href="forgot.htm">Forgot Password</a></td>
				</tr>					
			</table>
			</form:form>
			<c:if test="${error != null }"><li><font color="red">${error }</font></li></c:if>
		</div>
		
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>