<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<link href="../css/default.css" rel="stylesheet" type="text/css" media="screen" />

<html>
<head>
<script type="text/javascript">
function validateAmt()
{
	var amt = document.getElementById("amt").value;
	if((amt > 0) && (amt % 1 == 0))
	{
		if(amt > ${tailor.outstandingBal})
		{
			if(!confirm("You are paying more amount than the outstanding with this tailor.\nShall we proceed ?"))
				return;
		}
		
		document.forms[2].submit();
	}
}
</script>
</head>
<body>
<br>
<form:form method="post" action="pay.htm"  commandName="tailor">
<table style="background-color: grey">
	<tr>
		<td>Tailor ID: </td><td>${tailor.tailorId}<input type="hidden" name="tailorId" value="${tailor.tailorId}" /></td>
	</tr>
	<tr>
		<td>Tailor Name: </td><td>${tailor.name}</td>
	</tr>
	<tr>
		<td>Amount to Pay: </td><td><input type="text" id="amt" name="amt" /></td>
	</tr>
	<tr>
		<td><input type="button" class="button" value="PAY" onclick="javascript:validateAmt();" /></td>
	</tr>
</table>
</form:form>
</body>
</html>