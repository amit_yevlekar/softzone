<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="manageOrder"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Order Management</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Search Order</a></h2>			
			<form:form method="post" action="search.htm"  commandName="order">
			<table>
				<tr>
					<td>Search By: </td>
					<td><select name="searchCrit">
							<option value="Order Number">Order Number</option>
							<option value="Customer Name" ${searchCrit == 'Customer Name' ? 'selected' : '' }>Customer Name</option>							
						</select></td><td><input type="text" name="searchFld" title="Enter full or part of the value to be searched" value="${searchFld}" ></td>
					<td><input type="submit" class="button" value="Search"></td>
				</tr>				
			</table>
			<br/>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">List Order (${orderSize})</a></h2>
			<br>
			<c:url value="manageOrder.htm" var="pagedLink">  
				    <c:param name="action" value="list"/>  
				    <c:param name="p" value="~"/>  
			</c:url>
			<tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/>
			<br><br>
			<table border="1" >
				<c:choose>
				<c:when test="${orderSize == 0}"><tr><td><b>No Orders Found <c:if test="${searchCrit != null}">with that ${searchCrit}</c:if></b></td></tr></c:when>
				<c:otherwise>
					<tr>
						<td>Order No</td><td>Customer Name</td><td>No of Rooms</td><td>Status</td><td>Order Date</td><td>Delivery Date</td>
					</tr>
				</c:otherwise>
				</c:choose>
				
				<c:forEach items="${pagedListHolder.pageList}" var="orders" varStatus="status">
					<tr>
						<td><a href="updateOrder.htm?orderNo=${orders.orderNo}">${orders.orderNo}</a></td>
						<td>${orders.customerName}</td><td>${fn:length(orders.windowLst)}</td><td>${orders.status}</td>
						<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${orders.creationDate}" /></td><td>${orders.deliveryDate}</td>
						<td title="Delete"><a href="#" onclick="javascript:deleteOrder(${orders.orderNo}, ${fn:length(orders.windowLst)});"><img src="../images/Delete-icon.png" ></a></td>
						<c:if test="${orders.status == 'COMPLETED' }">
						<td title="Edit"><a href="#" onclick="javascript:reopenOrder(${orders.orderNo});"><img height="16" width="16" src="../images/Edit-icon.png" /></a></td>
						</c:if>
					</tr>
				</c:forEach>			
			</table>
			<br>
			<tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/>
			</form:form>
		</div>
		<p class="meta">Lost your Order ? Find it here !</p>
	</div>
	<!-- end content -->
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>