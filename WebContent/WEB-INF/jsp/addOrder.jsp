<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="addOrder"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<script>
	$(function() {
		
		var custName = [ ${custInfo} ];
		
	    $("input#autocomplete").autocomplete({
	        source: custName,
	        select: function( event, ui ) {
	        	document.getElementById("contactNo").value = ui.item.number;
	        	ui.item.address = ui.item.address.replace(/<br>/g, '\n');
	        	document.getElementById("address").value = ui.item.address;
	        }
	    });

	});

</script>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
		<c:choose>
			<c:when test="${orderInfo.status == 'COMPLETED' }">			
				<jsp:include page="/WEB-INF/jsp/calculate.jsp" />				
				<input type="button" class="button" value="Print Preview" onclick="javascript:printPreview(${orderInfo.orderNo});">
			</c:when>
			<c:otherwise>
			<h1 class="title">Order Management</h1>
			<c:choose>
			<c:when test="${not empty update}"><c:set var="AU" value="Update" /></c:when>
			<c:otherwise><c:set var="AU" value="Add" /></c:otherwise>
			</c:choose>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">${AU} Order</a></h2>
			<form:form method="post" action="saveOrder.htm"  commandName="order">
			<table>
				<tr>
					<td>Order No: </td><td><font size="4" color="FFEA6F">${orderInfo.orderNo}</font><input type="hidden" name="orderNo" value="${orderInfo.orderNo}" /></td>
					<td>Delivery Date: </td><td><input type="text" name="deliveryDate" id="deliveryDate" class="tcal" readonly="readonly" value="${orderInfo.deliveryDate}" ></td>
					<td>Status: </td><td>${orderInfo.status}<input type="hidden" name="status" value="${orderInfo.status}" /></td>
				</tr>
				<tr>
					<td>Customer Name: </td><td><input type="text" name="customerName" id="autocomplete" value="${orderInfo.customerName}" /></td>
					<td>Contact No: </td><td><input type="text" id="contactNo" name="contactNo" value="${orderInfo.contactNo}" maxlength="10" onkeyup="javascript:validatenumber(this);" /></td>
					<td>Address: </td><td><textarea cols="30" id="address" rows="4" name="address">${orderInfo.address}</textarea></td>
				</tr>
				<tr>
					<td>Tailor: </td>
					<td><select name="tailorId">
							<c:forEach items="${tailorLst}" var="tailors">
							<option ${tailors.tailorId == orderInfo.tailorId ? "selected" : "" } value="${tailors.tailorId}">${tailors.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<table border="1">
						<tr><td><b>Room Details: </b></td><td><c:if test="${fn:length(orderInfo.windowLst) == 0}">None</c:if></td></tr>
						<c:if test="${fn:length(orderInfo.windowLst) > 0}">
						<tr style="font-weight: bold;">
							<td>Name</td><td>Height (inch)</td><td>Width (inch)</td><td>Cloth Price</td><td>Stitch Style</td><td>Panels</td>
						</tr>
						<c:forEach items="${orderInfo.windowLst}" var="window" varStatus="status">
						<tr>
							<td><b>${window.name}</b></td><td>${window.height}</td><td>${window.width}</td><td>${window.price}</td><td>${stitchTypes[window.style - 1].name }</td><td>${window.panels}</td><td title="Edit"><a href="openUpdateWindow.htm?windowId=${window.windowId}&orderNo=${orderInfo.orderNo}"><img height="16" width="16" src="../images/Edit-icon.png" /></a></td><td title="Delete"><a href="#" onclick="javascript:deleteWindow(${orderInfo.orderNo}, ${window.windowId})"><img src="../images/Delete-icon.png" ></a></td>
						</tr>
						</c:forEach>
						</c:if>
					</table>
					</td>
				</tr>
				<c:if test="${fn:length(orderInfo.windowLst) > 0}">
				<tr>
					<td>Discount</td><td><input type="text" name="discount" value="${orderInfo.discount}" >%</td>
				</tr>
				</c:if>	
				<tr>
					<td colspan="3"><input type="submit" class="button" value="Save"><input type="button" class="button" value="Add Window" onclick="javascript:addWindow();"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<c:if test="${fn:length(orderInfo.windowLst) > 0}">				
				<tr>
					<td colspan="3"><input type="button" class="button" value="Calculate" onclick="javascript:calculate();"></td>
					<td colspan="3"><input type="button" class="button" value="Calculate Tailor Charges" onclick="javascript:calculateTailor();"></td>
				</tr>
				</c:if>
			</table>
			
			<c:if test="${not empty error}"><br><li><font color="red"><b>${error}</b></font></li></c:if>
			
			<p class="meta"></p>
			
			<c:if test="${not empty calculate}">
			<jsp:include page="/WEB-INF/jsp/calculate.jsp" />
			
			<input type="button" class="button" value="Print Preview" onclick="javascript:printPreview(${orderInfo.orderNo});">
			<input type="button" class="button" value="Finalize & Lock Order" onclick="javascript:finalizeNLock('${orderInfo.orderNo}');">
			</c:if>
			
			</form:form>
			</c:otherwise>
		</c:choose>
		</div>
		<p class="meta">Fulfill your orders here</p>
	</div>
	<!-- end content -->
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>