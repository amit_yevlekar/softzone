<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="addOrder"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<script type="text/javascript">
window.onload = function()
{
	styleChanged('${window.dblPrice}', '${window.dblTlrType}');
};
</script>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Order Management</h1>
			<c:choose>
				<c:when test="${not empty update}"><c:set var="AU" value="Update" /><c:set var="ACT" value="update" /></c:when>
				<c:otherwise><c:set var="AU" value="Add" /><c:set var="ACT" value="add" /></c:otherwise>
			</c:choose>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">${AU} Window</a></h2>
			<form:form method="post" action="${ACT}Window.htm"  commandName="order">
			<input type="hidden" name="orderNo" value="${orderNo}" >
			<input type="hidden" name="windowId" value="${window.windowId}" >
			<table id="winTable">
				<tr>
					<td>Window/Room Name: </td><td><input type="text" name="name" value="${window.name}" /></td>
				</tr>
				<tr>
					<td>Height (inch): </td><td><input type="text" id="height" name="height" value="${window.height}" onkeyup="javascript:validatenumber(this);" /></td><td><select name="heightScale"><option value="1">Inch</option><option value="12">Feet</option></select></td>
				</tr>
				<tr>
					<td>Width (inch): </td><td><input type="text" id="width" name="width" value="${window.width}" onkeyup="javascript:validatenumber(this);" onchange="javascript:calculatePanels();" /></td><td><select name="widthScale" id="widthScale" onchange="javascript:calculatePanels();"><option value="1">Inch</option><option value="12">Feet</option></select></td>
				</tr>
				<tr>
					<td>Stitch Style: </td>
					<td>
						<select name="style" onchange="javascript:calculatePanels();styleChanged('${window.dblPrice}', '${window.dblTlrType}');" id="style">
							<c:forEach items="${stitchTypes}" var="stitchType">
							<option ${stitchType.typeId == window.style ? "selected" : "" } value="${stitchType.typeId}">${stitchType.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>Panels Required: </td><td><input type="text" id="panels" <c:if test="${window.customPanels != 'true' }">style="background-color: #EEC591" readonly="readonly"</c:if> name="panels" value="${window.panels}" /> <input type="checkbox" <c:if test="${window.customPanels == 'true' }">checked="true"</c:if> name="customPanels" id="customPanels" onclick="javascript:editPanels(this);" title="Check this box to edit the calculated panels" /></td>
				</tr>
				<tr>
					<td>Additional Info: </td><td><textarea cols="30" rows="4" name="info">${window.info}</textarea></td>
				</tr>
				<tr>
					<td>Cloth Price: </td><td><input type="text" name="price" value="${window.price}" /></td>
				</tr>				
				<tr>
					<td><input type="submit" class="button" value="${AU}" /></td>
				</tr>
			</table>
			</form:form>
		</div>
		<p class="meta">Fulfill your orders here</p>
	</div>
	<!-- end content -->
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>