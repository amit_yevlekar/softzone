<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="addOrder"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<div align="center">
<form:form method="post" action="confirm.htm"  commandName="order">

<hr color="#4A3903" />

<br>

<c:set var="grandTotal" value="0" />
<font size="6" color="#FFEA6F">Tailor Charges</font>
<table width="830">
	<tr>
		<td align="left">Customer Name: <b>${orderInfo.customerName}</b></td><td align="right">Order Date: <b><fmt:formatDate pattern="dd-MMM-yyyy" value="${orderInfo.creationDate}" /></b></td>
	</tr>
	<tr>
		<td align="left">Address: <b>${orderInfo.address}</b></td><td align="right">Order No: <b>${orderInfo.orderNo}</b></td>
	</tr>
	<tr>
		<td align="left">Contact No: <b>${orderInfo.contactNo}</b></td><td align="right" nowrap="nowrap">Delivery Date: <b>${orderInfo.deliveryDate}</b></td>
	</tr>
</table>
<hr color="#4A3903" />
<br>
<table width="830">
	<c:forEach items="${orderInfo.windowLst}" var="window" varStatus="status">
	<tr>
		<td><h2><img height="16" width="16" src="../images/img05.gif"/><a href="#">${window.name}</a></h2></td>
		<td>
			<table width="100%">
			<tr><td>Height: <b>${window.height}</b> inch</td><td>Width: <b>${window.width}</b> inch</td><td>Pattern: <b>${stitchTypes[window.style - 1].name}</b></td><td>Panels: <b>${window.panels}</b></td></tr>
			<tr><td colspan="4">Details: <b>${window.info}</b></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<table style="border-style:double; " width="100%">
			<tr align="center" >
				<td width="40%" style="border-bottom: double;"><b>Description</b></td><td align="right" width="20%" style="border-bottom: double;"><b>Value</b></td>
			</tr>
						
			<tr><td>&nbsp;</td></tr>
			<tr align="center">
				<c:set var="grandTotal" value="${grandTotal+window.tailorCharges}" />
				<td>Tailor Charges</td><td align="right"><fmt:formatNumber type="number" pattern="${currFormat}" value="${window.tailorCharges}"/></td>
			</tr>			
		</table>
		</td>
	</tr>
	 
	</c:forEach>
	<tr>
	<td colspan="4">
	<table width="100%">
		
		<tr align="right">
			<td><h2>GRAND TOTAL:</h2></td><td><img src="../images/rs_white.png" width="16" height="20"></td><td><h2><fmt:formatNumber type="number" pattern="${currFormat}" value="${grandTotal}" /></h2></td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<input type="button" class="button" value="Back" onclick="javascript:history.back();">

<hr color="#4A3903" />
</form:form>
</div>