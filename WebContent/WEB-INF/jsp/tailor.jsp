<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="tailor"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Tailor Management</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Add Tailor</a></h2>
			<form:form method="post" action="add.htm"  commandName="tailor">
			<table>
				<tr>
					<td>ID :</td>
					<td><b id="tailorId">New</b><form:hidden path="tailorId"/></td>
				</tr>
				<tr>
					<td>Name :</td>
					<td><form:input path="name" /></td>
				</tr>	
				<tr>
					<td>Mobile :</td>
					<td><input id="mobNo" type="text" name="mobNo" maxlength="10" onkeyup="javascript:validatenumber(this);" /></td>
				</tr>
				<tr>
					<td>Address :</td>
					<td><form:textarea rows="4" cols="40" path="address" /></td>
				</tr>				
				<tr>
					<td>Outstanding Balance :</td>
					<td><form:input path="outstandingBal" /></td>
				</tr>
				<tr>
					<td>Paid Amount :</td>
					<td><form:input path="paidAmount" /></td>
				</tr>
				<tr>
					<td>Stitch Types :</td>
					<td>
						<table>
							<tr bgcolor="#87421F"><td>Stitch Type</td><td>Amount Calc</td><td>Amount Internal</td></tr>
							<c:forEach items="${lstStitchTypes}" var="stitchTypes" varStatus="status">
							<c:if test="${stitchTypes.name != 'Double'}">
							<tr>
								<td>${stitchTypes.name}<input type="hidden" id="stitchTypeId${status.count}" name="stitchTypeId" value="${stitchTypes.typeId }"></td><td><input type="text" id="stitchTypeAmount${status.count}" name="stitchTypeAmount" value="0.0"></td><td><input type="text" id="stitchTypeAmountInternal${status.count}" name="stitchTypeAmountInternal" value="0.0"></td>
							</tr>
							</c:if>
							</c:forEach>
						</table>
					</td>
				</tr>
				<tr>
					<td><input id="Add Tailor" type="submit" class="button" value="Add Tailor" onclick="javascript:checkTailorAction();"><input type="reset" class="button" value="Clear" onclick="javascript:doClearTailor();" /></td>
				</tr>	
			</table>
			</form:form>
		</div>
		<div class="post">
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">List of Tailors</a></h2>
			<div class="entry">
				<form:form action="delete.htm" commandName="tailor">
					<c:if test="${fn:length(tailorList) > 0}">
					<table cellpadding="5">					
						<tr>
							<th align="left" width="20"><input type="submit" class="button" value="Delete" /></th>
							<th>Name</th>
							<th>Mobile</th>
							<th>Outstanding Balance</th>
							<th>Paid Amount</th>
							<th colspan="2">Activity</th>
						</tr>
						
						<c:forEach items="${tailorList}" var="tailor" varStatus="status">
							<c:set var="stitchTypeInfo" value="" />
							<c:forEach items="${tailor.tlrStitchTyp}" var="tlrStitchTyp" varStatus="stat">
								<c:set var="stitchTypeInfo" value="${stitchTypeInfo}${tlrStitchTyp.pk.stitchType.typeId}${'::'}${tlrStitchTyp.amount}${stat.last ? '' : '||'}" />
							</c:forEach>
							<c:if test="${status.count % 2 == 0}">
							<tr>
							</c:if>
							<c:if test="${status.count % 2 != 0}">
							<tr bgcolor="#87421F">
							</c:if>
								<td><input type="checkbox" name="selectedTailor" value="${tailor.tailorId}" /></td>
								<td title="${tailor.address}"> ${tailor.name} </td>
								<td> ${tailor.mobNo} </td>
								<td> ${tailor.outstandingBal} </td>
								<td> ${tailor.paidAmount} </td>
								<td><input type="button" class="button" value="Update" onclick="javascript:fillTailorFields('${tailor.tailorId}', '${tailor.name}', '${tailor.mobNo}', '${tailor.address}', '${tailor.outstandingBal}', '${tailor.paidAmount}', '${stitchTypeInfo}');" /></td>
								<td><a class='ajax' href="payTailor.htm?id=${tailor.tailorId}">Pay</a></td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
				</form:form>
			</div>
			<p class="meta"><a href="#" class="more">Address will be displayed by hovering on the name</a> </p>
		</div>
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>