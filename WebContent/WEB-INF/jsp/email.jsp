<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="email"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<%
String emailBody = request.getParameter("emailTxt");
if(emailBody==null)
emailBody="";
else
{
	emailBody = emailBody.replaceAll("<br>", "\r\n");
}
%>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			
			<h1 class="title">Email</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Send Email</a></h2>
			<form:form method="post" action="sendEmail.htm"  commandName="email">
			<table>				
				<tr>
					<td>Email Address: </td><td><input title="Separate by semicolon(;) for multiple email addresses" type="text" name="emailAddr" value="${param.emailAddr}" size="100"/></td>					
				</tr>
				<tr>
					<td>Email Subject: </td><td><input type="text" id="subject" name="subject" value="${param.subject}" size="100"/></td>
				</tr>
				<tr>
					<td>Email Body: </td><td><textarea name="emailTxt" id="emailTxt" rows="10" cols="100"><%=emailBody %></textarea></td>
				</tr>
				<tr>
					<td colspan="2"><input class="button" type="submit" value="Send Email" /><input type="reset" class="button" value="Clear" onclick="javascript:doClearEmail();"/></td>
				</tr>
				
			</table>

			<c:if test="${param.error != null }"><li><font color="red">${param.error }</font></li></c:if>
			</form:form>
			
		</div>
		<p class="meta">Send your requests</p>
	</div>
	<!-- end content -->
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>