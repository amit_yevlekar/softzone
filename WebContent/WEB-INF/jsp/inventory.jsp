<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="inventory"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<script>
	$(function() {
		
		var suppName = [ ${suppInfo} ];
		
	    $("input#autocomplete").autocomplete({
	        source: suppName,
	        select: function( event, ui ) {
	        	document.getElementById("suppId").value = ui.item.id;	        	
	        }
	    });

	});

</script>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Inventory Management</h1>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Add Inventory Item</a></h2>
			<form:form method="post" action="add.htm"  commandName="inventory">
			<table>
				<tr>
					<td>Inventory ID :</td>
					<td><b id="inventoryId">New</b><form:hidden path="inventoryId"/></td>
				</tr>
				<tr>
					<td>Collection Name :</td>
					<td><form:input path="collectName" /></td>
				
					<td>Quality :</td>
					<td><form:input path="quality" /></td>
				
					<td>Shade :</td>
					<td><form:input path="shade" /></td>
				</tr>
				<tr>
					<td>Serial Number :</td>
					<td><form:input path="serialNo" /></td>
				
					<td>Supplier Ref Name :</td>
					<td><form:input path="suppRefName" id="autocomplete" /></td>
					
					<td>Supplier Id :</td>
					<td><form:input path="suppId" readonly="true" /></td>					
				</tr>				
				<tr>
					<td>Counter Roll Rate :</td>
					<td><form:input path="cntrRollRate" /></td>
				
					<td>Counter Cut Rate :</td>
					<td><form:input path="cntrCutRate" /></td>
				
					<td>MRP :</td>
					<td><form:input path="mrp" /></td>
				</tr>
				<tr>
					<td>Stock (Mtrs) Laxmi:</td>
					<td><form:input path="stockMtrsLaxmi" /> </td>
					<td>Stock (Mtrs) Kondhwa:</td>
					<td><form:input path="stockMtrsKondhwa" /></td>
				</tr>				
				<tr>
					<td colspan="3"><input id="Add To Inventory" type="submit" class="button" value="Add To Inventory" onclick="javascript:checkInventoryAction(this.form);"><input type="reset" class="button" value="Clear" onclick="javascript:doClearInventory(this.form);" /></td>
				</tr>
			</table>
			</form:form>
		</div>
		<div class="post">
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Search Order</a></h2>			
			<form:form method="post" action="search.htm"  commandName="inventory">
			<table>
				<tr>
					<td>Search By: </td>
					<td><select name="searchCrit">
							<option value="Collection Name">Collection Name</option>
							<option value="Serial No" ${searchCrit == 'Serial No' ? 'selected' : '' }>Serial No</option>							
						</select></td><td><input type="text" name="searchFld" title="Enter full or part of the value to be searched" value="${searchFld}" ></td>
					<td><input type="submit" class="button" value="Search"></td>
				</tr>				
			</table>
			</form:form>
			<br/>
			<h2 class="title"><img src="../images/img05.gif"/><a href="#">Inventory List (${inventorySize})</a></h2>
			<div class="entry">
			<br>
			<c:url value="inventory.htm" var="pagedLink">  
				    <c:param name="action" value="list"/>
				    <c:param name="p" value="~"/>  
			</c:url>
			<tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/>
			<br><br>
				<form:form action="delete.htm" commandName="inventory">
					<c:if test="${inventorySize > 0}">
					<table cellpadding="5">
						<tr>
							<th align="left" width="20"><input type="submit" class="button" value="Delete" /></th>
							<th>Collection Name</th>
							<th>Quality</th>
							<th>Shade</th>
							<th>Sr No</th>
							<th>Supplier</th>
							<th>Roll Rate</th>
							<th>Cut Rate</th>
							<th>MRP</th>
							<th>Stock Laxmi Rd</th>
							<th>Stock Kondhwa</th>							
						</tr>
						
						<c:forEach items="${pagedListHolder.pageList}" var="inventory" varStatus="status">
							
							<c:if test="${status.count % 2 == 0}">
							<tr>
							</c:if>
							<c:if test="${status.count % 2 != 0}">
							<tr bgcolor="#87421F">
							</c:if>
								<td><input type="checkbox" name="selectedInventory" value="${inventory.inventoryId}" /></td>
								<td> ${inventory.collectName} </td>
								<td> ${inventory.quality} </td>
								<td> ${inventory.shade} </td>
								<td> ${inventory.serialNo} </td>
								<td title="${inventory.suppId}"> ${inventory.suppRefName} </td>
								<td> ${inventory.cntrRollRate} </td>
								<td> ${inventory.cntrCutRate} </td>
								<td> ${inventory.mrp} </td>
								<td> ${inventory.stockMtrsLaxmi} </td>
								<td> ${inventory.stockMtrsKondhwa} </td>
								<td><input type="button" class="button" value="Update" onclick="javascript:fillInventoryFields('${inventory.inventoryId}', '${inventory.collectName}', '${inventory.quality}', '${inventory.shade}', '${inventory.serialNo}', '${inventory.suppId}', '${inventory.suppRefName}', '${inventory.cntrRollRate}', '${inventory.cntrCutRate}', '${inventory.mrp}', '${inventory.stockMtrsLaxmi}', '${inventory.stockMtrsKondhwa}');" /></td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
				</form:form>
				<br>
				<tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/>
				
			</div>
			<p class="meta"><a href="#" class="more">Invent something new...</a> </p>
		</div>
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>