<% 
	Object pageName = request.getAttribute("pageName");
	if(session.getAttribute("login")==null && pageName != null)
  	{
		
		if(("login,resetPassword,forgotPassword").indexOf(pageName.toString()) == -1)
		{
			if(!pageName.equals("login"))
			{
				response.sendRedirect("../login/start.htm");
				return;
			}			
		}		
  	}
	else if(session.getAttribute("login")!=null && pageName.equals("login"))
	{
		response.sendRedirect("../order/addOrder.htm");
		return;
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SoftZone</title>
<link rel="stylesheet" href="../css/colorbox.css" />
<link href="../css/default.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="../css/tcal.css" />
<link href="../css/humanity/jquery-ui-1.10.3.custom.css" rel="stylesheet">

<script src="../js/jquery.colorbox.js"></script>
<script type="text/javascript" src="../js/my.js"></script>
<script type="text/javascript" src="../js/tcal.js"></script>	
<script src="../js/jquery-1.9.1.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.js"></script>

</head>
<body>
<!-- start header -->
<div id="header">
	<div id="logo">
		<h1><a href="../login/start.htm">SoftZone </a></h1>
		<h2><a href="#">Order Management</a><div align="right"><c:if test="${sessionScope.login != null}" ><form action="../login/logout.htm" ><font size="3" color="black">Welcome, ${sessionScope.login} (${sessionScope.location})</font> <c:if test="${sessionScope.admin != null}" ><img height="15" width="15" alt="" src="../images/Admin.png"></c:if> <input type="submit" class="button" value="Logout"/></form></c:if></div></h2>
	</div>
	<c:if test="${sessionScope.login != null}" >
	<div id="menu">
		<ul>
			<li <c:if test="${pageName == 'addOrder'}">class="active"</c:if> ><a href="../order/addOrder.htm" accesskey="1" title="New Orders">Add Orders</a></li>
			<li <c:if test="${pageName == 'manageOrder'}">class="active"</c:if> ><a href="../order/manageOrder.htm" accesskey="2" title="Existing Orders">Manage Orders</a></li>
			<li <c:if test="${pageName == 'invoice'}">class="active"</c:if> ><a href="../invoice/addInvoice.htm" accesskey="3" title="Invoicing">Invoice</a></li>
			<li <c:if test="${pageName == 'tailor'}">class="active"</c:if> ><a href="../tailor/tailor.htm" accesskey="4" title="Tailor Management">Tailors</a></li>
			<li <c:if test="${pageName == 'customer'}">class="active"</c:if> ><a href="../customer/customer.htm" accesskey="5" title="Customer Management">Customer</a></li>
			<li <c:if test="${pageName == 'inventory'}">class="active"</c:if> ><a href="../inventory/inventory.htm" accesskey="6" title="Inventory Management">Inventory</a></li>
			<c:if test="${sessionScope.admin != null}" >
			<li <c:if test="${pageName == 'report'}">class="active"</c:if> ><a href="../report/report.htm" accesskey="7" title="Reports">Reports</a></li>
			<li <c:if test="${pageName == 'user'}">class="active"</c:if> ><a href="../user/user.htm" accesskey="8" title="Local User Management">User</a></li>
			<li <c:if test="${pageName == 'supplier'}">class="active"</c:if> ><a href="../supplier/supplier.htm" accesskey="9" title="Supplier Management">Supplier</a></li>
			</c:if>
		</ul>
	</div>
	</c:if>
</div>
<!-- end header -->