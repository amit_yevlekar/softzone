<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div align="center">
<form:form method="post" action="confirm.htm"  commandName="order">
<div align="center"><b><font color="#000" size="20">SOFT<font color="#C0C0C0">ZONE</font></font></b></div>
<hr color="#4A3903" />

<br>

<c:set var="grandTotal" value="0" />

<table width="830">
	<tr>
		<td align="left">Customer Name: <b>${orderInfo.customerName}</b></td><td align="right">Order Date: <b><fmt:formatDate pattern="dd-MMM-yyyy" value="${orderInfo.creationDate}" /></b></td>
	</tr>
	<tr>
		<td align="left">Address: <b>${orderInfo.address}</b></td><td align="right">Order No: <b>${orderInfo.orderNo}</b></td>
	</tr>
	<tr>
		<td align="left">Contact No: <b>${orderInfo.contactNo}</b></td><td align="right" nowrap="nowrap">Delivery Date: <b>${orderInfo.deliveryDate}</b></td>
	</tr>
</table>
<hr color="#4A3903" />
<br>
<table width="830">
	<c:forEach items="${orderInfo.windowLst}" var="window" varStatus="status">
	<tr>
		<td><h2><img height="16" width="16" src="../images/img05.gif"/><a href="#">${window.name}</a></h2></td>
		<td>
			<table width="100%">
			<tr><td>Height: <b>${window.height}</b> inch</td><td>Width: <b>${window.width}</b> inch</td><td>Pattern: <b>${stitchTypes[window.style - 1].name}</b></td><td>Panels: <b>${window.panels}</b></td></tr>
			<tr><td colspan="4">Details: <b>${window.info}</b></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<table style="border-style:double; " width="100%">
			<tr align="center" >
				<td width="40%" style="border-bottom: double;"><b>Description</b></td><td width="20%" style="border-bottom: double;"><b>Quantity (Mtrs)</b></td><td width="20%" style="border-bottom: double;"><b>Rate</b></td><td align="right" width="20%" style="border-bottom: double;"><b>Value</b></td>
			</tr>
			<tr align="center">
				<c:set var="clothPrice" value="${window.price * window.heightMtrs}" />
				<c:set var="grandTotal" value="${grandTotal+clothPrice}" />
				<td>Cloth Charges</td><td><fmt:formatNumber type="number" pattern="${currFormat}" value="${window.heightMtrs}"/></td><td>${window.price}</td><td align="right"><fmt:formatNumber type="number" pattern="${currFormat}" value="${clothPrice}"/></td>
			</tr>
			<c:set var="dblClothPrice" value="0" />
			<c:if test="${stitchTypes[window.style - 1].name == 'Double'}">
			<tr align="center">
				<c:set var="dblClothPrice" value="${window.dblPrice * window.heightMtrs}" />
				<c:set var="grandTotal" value="${grandTotal+dblClothPrice}" />
				<td>Double Side Cloth Charges</td><td><fmt:formatNumber type="number" pattern="${currFormat}" value="${window.heightMtrs}"/></td><td>${window.dblPrice}</td><td align="right"><fmt:formatNumber type="number" pattern="${currFormat}" value="${dblClothPrice}"/></td>
			</tr>
			</c:if>
			<c:if test="${stitchTypes[window.style - 1].name == 'Roman'}">
			<tr align="center">
				<c:set var="noOfChannel" value="${(window.width / 12)+(1-((window.width / 12)%1))%1}" />
				<c:set var="romanChannelTotal" value="${romanChannelPrice * noOfChannel}" />
				<c:set var="grandTotal" value="${grandTotal+romanChannelTotal}" />
				<td>Roman Channel Charges</td><td><fmt:formatNumber type="number" pattern="#" value="${noOfChannel}"/></td><td>${romanChannelPrice}</td><td align="right"><fmt:formatNumber type="number" pattern="${currFormat}" value="${romanChannelTotal}"/></td>
			</tr>
			</c:if>
			<c:if test="${orderInfo.discount > 0 }">
				<c:set var="discountPrice" value="${discountPrice + ((clothPrice + dblClothPrice) * orderInfo.discount / 100)}" />
			</c:if>
			<tr align="center">
				<c:set var="asterCharge" value="${aster *  window.heightMtrs}" />
				<c:set var="grandTotal" value="${grandTotal+asterCharge}" />
				<td>Aster Charges</td><td><fmt:formatNumber type="number" pattern="${currFormat}" value="${window.heightMtrs}"/></td><td>${aster}</td><td align="right"><fmt:formatNumber type="number" pattern="${currFormat}" value="${asterCharge}"/></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center">
				<c:set var="grandTotal" value="${grandTotal+window.tailorCharges}" />
				<td>Tailor Charges</td><td>&nbsp;</td><td>&nbsp;</td><td align="right"><fmt:formatNumber type="number" pattern="${currFormat}" value="${window.tailorCharges}"/></td>
			</tr>			
		</table>
		</td>
	</tr>
	 
	</c:forEach>
	<tr>
	<td colspan="4">
	<table width="100%">
		<tr align="right">
			<td><font size="3">TOTAL CHARGES:</font></td><td><img src="../images/rs_${color}.png" width="10" height="12.5"></td><td><font size="3"><fmt:formatNumber type="number" pattern="${currFormat}" value="${grandTotal}" /></font></td>
		</tr>
		<c:if test="${orderInfo.discount > 0 }">
		<tr align="right">
			<td><font size="3">DISCOUNTS @ ${orderInfo.discount}%:</font></td><td><img src="../images/rs_${color}.png" width="10" height="12.5"></td><td><font size="3"><fmt:formatNumber type="number" pattern="${currFormat}" value="${discountPrice}" /></font></td>
		</tr>
		</c:if>
		<tr align="right">
			<td><h2>GRAND TOTAL:</h2></td><td><img src="../images/rs_${color}.png" width="16" height="20"></td><td><h2><fmt:formatNumber type="number" pattern="${currFormat}" value="${grandTotal-discountPrice}" /></h2><input type="hidden" id="grTot" value="${grandTotal-discountPrice}"></td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<c:if test="${empty calculate}">
<a href="#" onclick="javascript:window.print();"><img alt="Click here to print this page" height="35" width="35" src="../images/print.png"/></a>
</c:if>
<hr color="#4A3903" />
</form:form>
</div>