<%@ include file="/WEB-INF/jsp/header.jsp" %>
<c:set var="pageName" scope="request" value="login"/>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Forgot Password</h1>
			<h2 class="title"><img src="../images/img05.gif"/>Login</h2>
			<form:form method="post" action="forgotPwd.htm"  commandName="login">
				<table cellpadding="0" cellspacing="2">
					<tr>
						<td>Username :</td>
						<td><input type="text" name="username" /></td>						
					</tr>					
					<tr>
						<td colspan="2"><input type="submit" value="Mail Password" /></td>
					</tr>
				</table>
				<br>
				<c:if test="${error != null }"><li><font color="red">${error }</font></li></c:if>
			</form:form>
		</div>
		
	</div>
	<!-- end content -->	
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>