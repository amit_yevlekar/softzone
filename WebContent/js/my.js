String.prototype.replaceAll = function(search, replace, ignoreCase) {
  if (ignoreCase) {
    var result = [];
    var _string = this.toLowerCase();
    var _search = search.toLowerCase();
    var start = 0, match, length = _search.length;
    while ((match = _string.indexOf(_search, start)) >= 0) {
      result.push(this.slice(start, match));
      start = match + length;
    }
    result.push(this.slice(start));
  } else {
    result = this.split(search);
  }
  return result.join(replace);
}

//COLORBOX
$(document).ready(function(){
	//Examples of how to assign the ColorBox event to elements
	$(".group1").colorbox({rel:'group1'});
	$(".group2").colorbox({rel:'group2', transition:"fade"});
	$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
	$(".group4").colorbox({rel:'group4', slideshow:true});
	$(".ajax").colorbox();
	$(".youtube").colorbox({iframe:true, innerWidth:425, innerHeight:344});
	$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
	$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	$(".inline").colorbox({inline:true, width:"50%"});
	$(".callbacks").colorbox({
		onOpen:function(){ alert('onOpen: colorbox is about to open'); },
		onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
		onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
		onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
		onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
	});
	
	//Example of preserving a JavaScript event for inline calls.
	$("#click").click(function(){ 
		$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
		return false;
	});
});


function fillTailorFields(tailorId, name, mobNo, address, outstandingBal, paidAmount, stitchTypeInfo)
{
	var stitchTypes = stitchTypeInfo.split("||");

	for ( var int = 0; int < stitchTypes.length; int++) 
	{
		var data = stitchTypes[int].split("::");
		//alert(data[0]+"--"+data[1]);
		try{
		document.getElementById("stitchTypeAmount"+data[0]).value = data[1];
		document.getElementById("stitchTypeAmountInternal"+data[0]).value = data[2];
		}catch(err){}
	}
	
	var form_element = document.forms[1];
	document.getElementById("tailorId").innerHTML = tailorId;
	form_element.tailorId.value = tailorId;
	form_element.name.value = name;
	form_element.mobNo.value = mobNo;
	if(address.indexOf("<br>") != -1)
	{
		address = address.replaceAll('<br>', '\r\n');
	}
	form_element.address.value = address;
	form_element.outstandingBal.value = outstandingBal;
	form_element.paidAmount.value = paidAmount;
			
	document.getElementById("Add Tailor").value="Update Tailor";
	return;
}

function doClearTailor()
{
	if(document.getElementById("Add Tailor").value=="Update Tailor")
	{		
		document.getElementById("Add Tailor").value="Add Tailor";
		document.getElementById("tailorId").innerHTML = "New";
	}
}

function checkTailorAction()
{
	if(document.getElementById("Add Tailor").value=="Update Tailor")
	{
		var form_element = document.forms[1];
		form_element.action = "update.htm";
	}
}

function payTailor()
{
	var form_element = document.forms[1];
	form_element.action = "payTailor.htm";	
}

function validatenumber(inputtxt)
{	
	//var inputtxt = document.getElementById("mobNo");
	
	if ( inputtxt.value % 1 == 0 )
        return true;
	else
    {
		inputtxt.value = "";
		alert("You can use only numbers !!!");
		return false;
    }
}

function checkEmail(email) 
{
    //var email = document.getElementById('txtEmail');
    if(email.value == '')
    	return true;
    
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
    alert('Please provide a valid email address');
    email.focus();
    return false;
 }
}

function addWindow()
{
	var form_element = document.forms[1];
	form_element.action = "openWindow.htm";
	form_element.submit();
}

function saveWindow(act)
{
	var form_element = document.forms[1];
	if(act=='Update')
		form_element.action = "updateWindow.htm";
	
	form_element.submit();
}

function calculate()
{
	var form_element = document.forms[1];
	form_element.action = "calculate.htm";	
	form_element.submit();
}

function calculateTailor()
{
	var form_element = document.forms[1];
	form_element.action = "calculateTailor.htm";	
	form_element.submit();
}
	
function deleteWindow(orderNo, windowId)
{	
	if(confirm("Window will be deleted. Are you sure ?"))
	{
		var form_element = document.forms[1];
		form_element.action = "deleteWindow.htm?windowId="+windowId+"&orderNo="+orderNo;	
		form_element.submit();
	}	
}

function deleteOrder(orderNo, windows)
{
	if(windows == 0)
	{
		if(confirm("Order No. "+ orderNo +" will be deleted. Are you sure ?"))
		{
			var form_element = document.forms[1];
			form_element.action = "deleteOrder.htm?orderNo="+orderNo;	
			form_element.submit();
		}
	}
	else
	{
		alert("Order No. "+ orderNo +" has "+windows+" window/s added.\nPlease delete the windows by opening the order before you delete the order.");
	}
}

function calculatePanels()
{
	var selCtrl = document.getElementById("style");
	var width = parseInt(document.getElementById("width").value);
	var widthScale = parseInt(document.getElementById("widthScale").value);

	var stitch = selCtrl.options[selCtrl.value - 1].text;	
	
	if(("Eylet,A/P,Double").indexOf(stitch) != -1)
	{
		var panels = Math.ceil(width*widthScale / 20);
		document.getElementById("panels").value = panels;
	}
	else
	{
		var panels = Math.ceil(width*widthScale / 40);
		document.getElementById("panels").value = panels;
	}
	
	var panelCtrl = document.getElementById("panels");
	panelCtrl.readOnly = true;
	panelCtrl.style.backgroundColor = "#EEC591";
	document.getElementById("customPanels").checked = false;
}

function styleChanged(price, dblTlrType)
{
	var selCtrl = document.getElementById("style");
	var stitch = selCtrl.options[selCtrl.value - 1].text;
	
	if(stitch == "Double")
	{
		var tab = document.getElementById("winTable");
		var row = tab.insertRow(7);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	    cell1.innerHTML = 'Double Cloth Price: ';
	    cell2.innerHTML = '<input type="text" id="dblPrice" name="dblPrice" /><select id="dblTlrType" name="dblTlrType"><option value="Eylet" >Eylet, A/P</option><option value="A/P" >A/P, A/P</option></select>';
	    if(price == undefined) price = "0";
	    document.getElementById("dblPrice").value = price;
	    var sel = document.getElementById("dblTlrType");
	    if(dblTlrType == "A/P")
	    	sel.selectedIndex = 1;
	}
	else
	{
		var loc = document.getElementById("dblPrice");
		if(loc != null)
		{
			var tab = document.getElementById("winTable");
			tab.deleteRow(7);
		}
	}
}

function editPanels(ckBox)
{
	var panelCtrl = document.getElementById("panels");
	if(ckBox.checked == true)
	{	
		panelCtrl.readOnly = false;
		panelCtrl.removeAttribute("style");
	}
	else
	{
		panelCtrl.readOnly = true;
		panelCtrl.style.backgroundColor = "#EEC591";
	}	
}

function printPreview(orderNo)
{
	window.open("calculate.htm?printPreview=true&orderNo="+orderNo);
}

function finalizeNLock(orderNo)
{
	if(confirm("Order will be updated to completed.\nYou will be able to update it later from order list.\nAre you sure ?"))
	{
		var totalCharges = document.getElementById("grTot").value;
		var form_element = document.forms[1];
		form_element.action = "completeOrder.htm?orderNo="+orderNo+"&totalCharges="+totalCharges;
		form_element.submit();
	}
}

function reopenOrder(orderNo)
{
	if(confirm("Order status will be changed to REOPEN\nAre you sure order should be reopened ?"))
	{
		var form_element = document.forms[1];
		form_element.action = "reopenOrder.htm?orderNo="+orderNo+"&status=REOPEN";
		form_element.submit();
	}
}

function checkUserAction(form_element)
{
	if(document.getElementById("Add User").value == "Update User")
	{		
		form_element.action = "update.htm";
	}
}

function fillUserFields(username,name,email,isAdmin)
{	
	var form_element = document.forms[1];
	form_element.username.value = username;
	form_element.username.readOnly = true;
	form_element.name.value = name;
	form_element.email.value = email;

	if(isAdmin == "true")
	{
		form_element.isAdmin.checked = true;
	}
	else
	{
		form_element.isAdmin.checked = false;
	}
				
	document.getElementById("Add User").value="Update User";
}

function doClearUser(form_element)
{
	if(document.getElementById("Add User").value=="Update User")
	{
		//var form_element = document.forms[1];
		form_element.username.readOnly = false;		
		document.getElementById("Add User").value="Add User";
	}
}

function fillCustomerFields(custId, custName, custAddr, custMobile, custEmail)
{	
	var form_element = document.forms[1];
	form_element.custId.value = custId;
	document.getElementById("custId").innerHTML = custId;
	form_element.custName.value = custName;
	if(custAddr.indexOf("<br>") != -1)
	{
		custAddr = custAddr.replaceAll('<br>', '\r\n');
	}
	form_element.custAddr.value = custAddr;
	form_element.custMobile.value = custMobile;
	form_element.custEmail.value = custEmail;
	
				
	document.getElementById("Add Customer").value="Update Customer";
}

function doClearCustomer(form_element)
{
	if(document.getElementById("Add Customer").value=="Update Customer")
	{
		document.getElementById("custId").innerHTML = "New";
		document.getElementById("Add Customer").value="Add Customer";
	}
}

function checkCustomerAction(form_element)
{
	if(document.getElementById("Add Customer").value == "Update Customer")
	{		
		form_element.action = "update.htm";
	}
}

function fillSupplierFields(suppId, suppName, suppCode, suppRate, suppMobileNo, suppEmail, softzoneCode)
{	
	var form_element = document.forms[1];
	form_element.suppId.value = suppId;
	document.getElementById("suppId").innerHTML = suppId;
	form_element.suppName.value = suppName;
	form_element.suppCode.value = suppCode;
	form_element.suppRate.value = suppRate;
	form_element.suppMobileNo.value = suppMobileNo;
	form_element.suppEmail.value = suppEmail;
	form_element.softzoneCode.value = softzoneCode;
	
	form_element.suppId.readOnly = true;
				
	document.getElementById("Add Supplier").value="Update Supplier";
}

function doClearSupplier(form_element)
{
	if(document.getElementById("Add Supplier").value=="Update Supplier")
	{
		document.getElementById("suppId").innerHTML = "New";
		document.getElementById("Add Supplier").value="Add Supplier";
	}
}

function checkSupplierAction(form_element)
{
	if(document.getElementById("Add Supplier").value == "Update Supplier")
	{		
		form_element.action = "updateSupplier.htm";
	}
}

function fillInventoryFields(inventoryId, collectName, quality, shade, serialNo, suppId, suppRefName, cntrRollRate, cntrCutRate, mrp, stockMtrsLaxmi, stockMtrsKondhwa)
{	
	var form_element = document.forms[1];
	
	document.getElementById("inventoryId").innerHTML = inventoryId;
	
	form_element.inventoryId.value = inventoryId;
	form_element.collectName.value = collectName;
	form_element.quality.value = quality;
	form_element.shade.value = shade;
	form_element.serialNo.value = serialNo;
	form_element.suppId.value = suppId;
	form_element.suppRefName.value = suppRefName;
	form_element.cntrRollRate.value = cntrRollRate;
	form_element.cntrCutRate.value = cntrCutRate;
	form_element.mrp.value = mrp;
	form_element.stockMtrsLaxmi.value = stockMtrsLaxmi;
	form_element.stockMtrsKondhwa.value = stockMtrsKondhwa;
					
	document.getElementById("Add To Inventory").value="Update Inventory";
}

function doClearInventory(form_element)
{
	if(document.getElementById("Add To Inventory").value=="Update Inventory")
	{
		document.getElementById("inventoryId").innerHTML = "New";		
		document.getElementById("Add To Inventory").value="Add To Inventory";
	}
}

function checkInventoryAction(form_element)
{
	if(document.getElementById("Add To Inventory").value == "Update Inventory")
	{		
		form_element.action = "update.htm";
	}
}

function doClearEmail()
{
	document.getElementById("subject").value="";
	document.getElementById("emailTxt").value = "";
}