package com.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.SupplierDAO;
import com.domain.Supplier;

public class SupplierController extends MultiActionController {

	private SupplierDAO suppDAO;
		
	private BindingResult errors;
	
	public BindingResult getErrors() {
		return errors;
	}

	public void setErrors(BindingResult errors) {
		this.errors = errors;
	}

	public void setSuppDAO(SupplierDAO suppDAO) {
		this.suppDAO = suppDAO;
	}
	
	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Supplier supplier) throws Exception 
	{
		//Validate Supplier fields
		validateSupplierFields();
		
		try
		{
			suppDAO.saveSupplier(supplier);			
		}
		catch (DataIntegrityViolationException e) 
		{
			System.out.println("The Supplier is existing:"+e);
			errors.addError(new ObjectError("IntegrityViolation", "The Supplier is already taken"));
			return new ModelAndView("redirect:supplier.htm", errors.getModel());
		}
		
		return new ModelAndView("redirect:supplier.htm");
	}
	
	private void validateSupplierFields() {
		
	}

	public ModelAndView delete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String [] values = request.getParameterValues("selectedSupplier");
		
		//List idList = new ArrayList();
		for (int j = 0; values!=null && j < values.length; j++) 
		{			
			suppDAO.deleteSupplier(new Supplier(Long.parseLong(values[j])));
		}
				
		return new ModelAndView("redirect:supplier.htm");
	}
	
	public ModelAndView updateSupplier(HttpServletRequest request,
			HttpServletResponse response, Supplier supplier) throws Exception {
		
		suppDAO.updateSupplier(supplier);
		
		return new ModelAndView("redirect:supplier.htm");
	}

	public ModelAndView supplier(HttpServletRequest request,
			HttpServletResponse response, Supplier supplier) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("supplierList", suppDAO.listSupplier());
		//modelMap.addAttribute("hierUsers", userDAO.createHierarchy("0", new ArrayList(), " "));
		modelMap.addAttribute("supplier", supplier);
		return new ModelAndView("supplier", modelMap);
	}
}