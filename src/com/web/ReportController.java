package com.web;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.OrderDAO;
import com.dao.TailorDAO;
import com.domain.Tailor;

public class ReportController extends MultiActionController {

	private OrderDAO orderDAO;
	private TailorDAO tailorDAO;
	
	public void setTailorDAO(TailorDAO tailorDAO) {
		this.tailorDAO = tailorDAO;
	}
	
	public void setOrderDAO(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}
	
	public ModelAndView report(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		
		String reportType = request.getParameter("reportType");
		if(reportType!=null)
		{
			modelMap.addAttribute("ordYrs", orderDAO.getOrderYears());
			modelMap.addAttribute("reportType", reportType);
		}
		
		return new ModelAndView("reports", modelMap);
	}
	
	public ModelAndView openReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		
		String reportType = request.getParameter("reportType");
		StringBuilder jsData = new StringBuilder();
		StringBuilder category = new StringBuilder();
		if(reportType.equals("Turn Over"))
		{
			String year = request.getParameter("year");
			modelMap.addAttribute("yearSel", year);
			List<String[]> lstResp = orderDAO.getTurnOver(year);
			jsData.append("{name: 'Amount',data: [");
			for (int i = 0; i < lstResp.size(); i++) 
			{
				String[] data = lstResp.get(i);
				category.append("'"+getDateGui(data[0])+"',");				
				jsData.append(data[1]+",");
			}
			if(lstResp.size()>0)
			{
				jsData.deleteCharAt(jsData.length()-1).append("]}");
				category.deleteCharAt(category.length()-1);
			}
			
			modelMap.addAttribute("ordYrs", orderDAO.getOrderYears());
			modelMap.addAttribute("metric", "Rs");
			modelMap.addAttribute("yAxisHeader", "Amount");
			modelMap.addAttribute("title", "SoftZone Turn Over");
		}
		else if(reportType.equals("Tailor Payment"))
		{
			List<Tailor> lstTailor = tailorDAO.listTailor();
			//String categories = "'Name', 'Outstanding Amount', 'Paid Amount'";
			
			StringBuilder outstandingBal = new StringBuilder();
			StringBuilder paidAmt = new StringBuilder();
			outstandingBal.append("{name: 'Outstanding Amount',data: [");
			paidAmt.append("{name: 'Paid Amount',data: [");
			for (int i = 0; i < lstTailor.size(); i++) 
			{
				Tailor tailor = lstTailor.get(i);
				category.append("'"+tailor.getName()+"',");
				outstandingBal.append(tailor.getOutstandingBal()+",");
				paidAmt.append(tailor.getPaidAmount()+",");
			}
			outstandingBal.deleteCharAt(outstandingBal.length()-1).append("]}");
			paidAmt.deleteCharAt(paidAmt.length()-1).append("]}");
			jsData.append(outstandingBal).append(",").append(paidAmt);
			if(category.length()>0)
				category.deleteCharAt(category.length()-1);

			modelMap.addAttribute("metric", "Rs");
			modelMap.addAttribute("yAxisHeader", "Amount");
			modelMap.addAttribute("title", "Tailor Payment");
		}
		else if(reportType.equals("Order Status"))
		{
			String year = request.getParameter("year");
			modelMap.addAttribute("yearSel", year);
			List<String[]> lstResp = orderDAO.getOrderStatus(year);
			
			LinkedHashMap hmDat = new LinkedHashMap();
			String[] arrDat = null;
			for (int i = 0; i < lstResp.size(); i++)
			{
				String[] data = lstResp.get(i);
				String date = getDateGui(data[0]);
				if(hmDat.containsKey(date))
				{
					arrDat = (String[]) hmDat.get(date);					
				}
				else
				{
					arrDat = new String[]{"0","0","0"};				
				}
				
				if(data[1].equals("NEW"))
				{
					arrDat[0] = data[2];
				}
				else if(data[1].equals("REOPEN"))
				{
					arrDat[1] = data[2];
				}
				else if(data[1].equals("COMPLETED"))
				{
					arrDat[2] = data[2];
				}
									
				hmDat.put(date, arrDat);
			}
			
			
			StringBuilder compStat = new StringBuilder();
			StringBuilder newStat = new StringBuilder();
			StringBuilder reOpStat = new StringBuilder();
			compStat.append("{name: 'Completed Orders',data: [");
			newStat.append("{name: 'New Orders',data: [");
			reOpStat.append("{name: 'Re-Open Orders',data: [");
			
			Set<String> keys = hmDat.keySet();
			int cnt = 0;
			for (Iterator iterator = keys.iterator(); iterator.hasNext();) 
			{
				String key = (String) iterator.next();
				String[] dat = (String[]) hmDat.get(key);
				
				category.append("'"+key+"',");
				compStat.append(dat[2]+",");
				newStat.append(dat[0]+",");
				reOpStat.append(dat[1]+",");
				cnt++;
			}
			if(cnt>0)
			{
				compStat.deleteCharAt(compStat.length()-1).append("]}");
				newStat.deleteCharAt(newStat.length()-1).append("]}");
				reOpStat.deleteCharAt(reOpStat.length()-1).append("]}");				
				category.deleteCharAt(category.length()-1);
			}
			jsData.append(compStat).append(",").append(newStat).append(",").append(reOpStat);
			
			modelMap.addAttribute("title", "Order Status");
			modelMap.addAttribute("ordYrs", orderDAO.getOrderYears());
		}		
		
		modelMap.addAttribute("category", category.toString());
		modelMap.addAttribute("jsData", jsData.toString());
		modelMap.addAttribute("report", "report");
		modelMap.addAttribute("reportType", reportType);
		
		return new ModelAndView("reports", modelMap);
	}
	
	
	
	private String getDateGui(String inputYr)
	{
		if(inputYr.length() != 4)
			return "";
		
		int mm = Integer.parseInt(inputYr.substring(2, 4));
		String mon = "";
		switch (mm) {
		case 1:
			mon = "Jan";
			break;
		case 2:
			mon = "Feb";
			break;
		case 3:
			mon = "Mar";
			break;
		case 4:		
			mon = "Apr";
			break;
		case 5:	
			mon = "May";
			break;
		case 6:
			mon = "Jun";
			break;
		case 7:
			mon = "Jul";
			break;
		case 8:
			mon = "Aug";
			break;
		case 9:
			mon = "Sep";
			break;
		case 10:
			mon = "Oct";
			break;
		case 11:
			mon = "Nov";
			break;		
		default:
			mon = "Dec";
			break;
		}
		
		return mon + "-20" + inputYr.substring(0, 2);
		
	}

}