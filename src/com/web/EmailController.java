package com.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.UserDAO;
import com.domain.Config;
import com.domain.User;
import com.helper.Encryption;
import com.helper.SendEmail;
import com.helper.TempPasswordGenerator;

public class EmailController extends MultiActionController {

	private static Config config;
	
	static
	{
		config = (Config)new ClassPathXmlApplicationContext("config.xml").getBean("config");
	}
	
	public ModelAndView email(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		return new ModelAndView("email");
	}
			
	public ModelAndView sendEmail(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		String emailTo = request.getParameter("emailAddr");
		String subject = request.getParameter("subject");
		String emailTxt = request.getParameter("emailTxt");
		emailTxt = emailTxt.replaceAll("\r\n", "<br>");
		
		try{
		SendEmail.sendEmail(emailTo.split(";"), subject,emailTxt, config);
		}catch(Exception me)
		{
			String error = "Unable to send email to "+emailTo;
			
			return new ModelAndView("redirect:email.htm?error="+error+"&emailAddr="+emailTo+"&subject="+subject+"&emailTxt="+emailTxt);
		}
			
		return new ModelAndView("redirect:email.htm?error=Message Sent");
	}
	

}