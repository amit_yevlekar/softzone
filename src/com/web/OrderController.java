package com.web;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.CustomerDAO;
import com.dao.OrderDAO;
import com.dao.TailorDAO;
import com.domain.Config;
import com.domain.Customer;
import com.domain.OrderInfo;
import com.domain.StitchType;
import com.domain.Tailor;
import com.domain.TailorStitchId;
import com.domain.TailorStitchType;
import com.domain.Window;

public class OrderController extends MultiActionController {

	private OrderDAO orderDAO ;
	private TailorDAO tailorDAO;
	private CustomerDAO custDAO;
	
	
	public void setOrderDAO(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}
	
	public void setTailorDAO(TailorDAO tailorDAO) {
		this.tailorDAO = tailorDAO;
	}
	
	public void setCustDAO(CustomerDAO custDAO) {
		this.custDAO = custDAO;
	}
	
	public ModelAndView addOrder(HttpServletRequest request,
			HttpServletResponse response, OrderInfo orderInfo) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		if(orderInfo.getOrderNo()==null)
		{
			orderInfo = new OrderInfo(orderDAO.generateId());
			orderInfo.setStatus("NEW");
			modelMap.addAttribute("orderInfo", orderInfo);
		}
		else
			modelMap.addAttribute("orderInfo",orderInfo);
		
		modelMap.addAttribute("tailorLst",tailorDAO.listTailor());
		modelMap.addAttribute("stitchTypes",tailorDAO.listStitches());		
		
		modelMap.addAttribute("custInfo",loadCustInfo());
		
		return new ModelAndView("addOrder", modelMap);
	}
	
	private String loadCustInfo()
	{
		List<Customer> customers = custDAO.listCustomer();
		StringBuilder sbData = new StringBuilder();
		for (int i = 0; i < customers.size(); i++) 
		{
			sbData.append("{ value: \"")
				.append((customers.get(i)).getCustName())
				.append("\",label: \"")
				.append((customers.get(i)).getCustName())
				.append("\", number: \"").append((customers.get(i)).getCustMobile()).append("\", address: \"").append((customers.get(i)).getCustAddr());
			
			if(i+1 != customers.size())
				sbData.append("\"},");
			else
				sbData.append("\"}");
		}
		
		return sbData.toString();
	}
	
	public ModelAndView saveOrder(HttpServletRequest request,
			HttpServletResponse response, OrderInfo orderInfo) throws Exception 
	{
		orderDAO.saveOrUpdateOrder(orderInfo);
		
		return updateOrder(request, response);
	}
	
	public ModelAndView updateOrder(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		String orderNo = request.getParameter("orderNo");
		OrderInfo orderInfo = orderDAO.getOrderById(orderNo);
		
		if(orderInfo.getStatus().equals("COMPLETED"))
		{
			return calculate(request, response, orderInfo);
		}
		
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("orderInfo",orderInfo);
		modelMap.addAttribute("tailorLst",tailorDAO.listTailor());
		modelMap.addAttribute("stitchTypes",tailorDAO.listStitches());
		modelMap.addAttribute("custInfo",loadCustInfo());
		modelMap.addAttribute("update","Update");
				
		return new ModelAndView("addOrder", modelMap);
	}
	
	
	public ModelAndView openWindow(HttpServletRequest request,
			HttpServletResponse response, OrderInfo orderInfo) throws Exception 
	{		
		orderDAO.saveOrUpdateOrder(orderInfo);
		
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("orderNo", orderInfo.getOrderNo());
		modelMap.addAttribute("window", new Window());
		modelMap.addAttribute("stitchTypes",tailorDAO.listStitches());
		
		return new ModelAndView("windowDetails", modelMap);
	}
	
	public ModelAndView openUpdateWindow(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{		
		ModelMap modelMap = new ModelMap();
		
		//orderDAO.saveOrUpdateOrder(orderInfo);
		Long windowId = Long.parseLong(request.getParameter("windowId"));
		String orderNo = request.getParameter("orderNo");
		Window window = orderDAO.getWindowById(windowId);
		
		modelMap.addAttribute("orderNo", orderNo);
		modelMap.addAttribute("window", window);
		modelMap.addAttribute("stitchTypes",tailorDAO.listStitches());
		modelMap.addAttribute("update","Update");
		
		return new ModelAndView("windowDetails", modelMap);
	}
	
	public ModelAndView addWindow(HttpServletRequest request,
			HttpServletResponse response, Window window) throws Exception
	{		
		String orderNo = request.getParameter("orderNo");
		int heightScale = Integer.parseInt(request.getParameter("heightScale"));
		int widthScale = Integer.parseInt(request.getParameter("widthScale"));
		window.setHeight(heightScale * window.getHeight());
		window.setWidth(widthScale * window.getWidth());
		
		OrderInfo orderInfo = orderDAO.getOrderById(orderNo);
		window.setOrderInfo(orderInfo);
		orderInfo.getWindowLst().add(window);
		
		orderDAO.saveOrUpdateOrder(orderInfo);
		orderDAO.saveOrUpdateWindow(window);
		
		//return addOrder(request, response, orderInfo);
		return new ModelAndView("redirect:updateOrder.htm?orderNo="+orderInfo.getOrderNo());
	}
	
	public ModelAndView updateWindow(HttpServletRequest request,
			HttpServletResponse response, Window window) throws Exception 
	{
		String orderNo = request.getParameter("orderNo");
		OrderInfo orderInfo = orderDAO.getOrderById(orderNo);
		int heightScale = Integer.parseInt(request.getParameter("heightScale"));
		int widthScale = Integer.parseInt(request.getParameter("widthScale"));
		window.setHeight(heightScale * window.getHeight());
		window.setWidth(widthScale * window.getWidth());
		
		window.setOrderInfo(orderInfo);
		
		window = orderDAO.mergeWindow(window);
				
		orderInfo = orderDAO.getOrderById(orderNo);
				
		return addOrder(request, response, orderInfo);
	}
	
	public ModelAndView deleteWindow(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		Long windowId = Long.parseLong(request.getParameter("windowId"));
		Window window = orderDAO.getWindowById(windowId);
		String orderNo = request.getParameter("orderNo");
		OrderInfo orderInfo = orderDAO.getOrderById(orderNo);
		
		orderDAO.deleteWindow(window);
		
		orderInfo = orderDAO.getOrderById(orderNo);
		
		//return addOrder(request, response, orderInfo);
		return new ModelAndView("redirect:updateOrder.htm?orderNo="+orderInfo.getOrderNo());
	}
	
	public ModelAndView manageOrder(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ModelMap modelMap = new ModelMap();
		List<OrderInfo> lstOrders = orderDAO.lstOrders();
		PagedListHolder pagedListHolder = new PagedListHolder(lstOrders);
		pagedListHolder.setPageSize(20);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);  
        pagedListHolder.setPage(page);
        modelMap.addAttribute("pagedListHolder", pagedListHolder);
		modelMap.addAttribute("orderSize", lstOrders.size());
				
		return new ModelAndView("searchOrder", modelMap);
	}
	
	public ModelAndView deleteOrder(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{		
		String orderNo = request.getParameter("orderNo");
		
		orderDAO.deleteOrder(new OrderInfo(orderNo));
		
		return manageOrder(request, response);		
	}
	
	
	public ModelAndView calculate(HttpServletRequest request,
			HttpServletResponse response, OrderInfo orderInfo) throws Exception
	{
		String page = "addOrder";
		ModelMap modelMap = new ModelMap();
		
		if(request.getParameter("printPreview") != null)
		{
			page = "calculate";
			orderInfo = new OrderInfo(request.getParameter("orderNo"));
			modelMap.addAttribute("calculate", "");
			modelMap.addAttribute("color", "black");
		}
		else
		{
			modelMap.addAttribute("calculate", "calculate");
			modelMap.addAttribute("color", "white");
		}
				
		Config config = (Config)new ClassPathXmlApplicationContext("config.xml").getBean("config");
		modelMap.addAttribute("aster", config.getAster());
		modelMap.addAttribute("romanChannelPrice", config.getRomanChannel());
		modelMap.addAttribute("currFormat", "#,##,##,##0.00");

		orderInfo = orderDAO.getOrderById(orderInfo.getOrderNo());
		
		//orderInfo.setDeliveryDate(dateFormatter(orderInfo.getDeliveryDate()));
		
		modelMap.addAttribute("orderInfo",orderInfo);
		List<Tailor> lstTailor = tailorDAO.listTailor();
		modelMap.addAttribute("tailorLst",lstTailor);
		List<StitchType> lstStitchType = tailorDAO.listStitches();
		modelMap.addAttribute("stitchTypes",lstStitchType);
		modelMap.addAttribute("update","Update");
		
		Tailor tailor = null;
		try{
		tailor = tailorDAO.getTailorById(orderInfo.getTailorId());
		}catch(IndexOutOfBoundsException e){
			modelMap.addAttribute("error","The tailor assigned to this order was deleted. Please select another tailor and save the order.");
			modelMap.remove("calculate");
			return new ModelAndView(page, modelMap);
		}
		
		//Calculation logic goes here
		amtCalculation(orderInfo.getWindowLst(), lstStitchType, tailor, true);
			
		return new ModelAndView(page, modelMap);
	}
	
	public ModelAndView calculateTailor(HttpServletRequest request,
			HttpServletResponse response, OrderInfo orderInfo) throws Exception
	{
		ModelMap modelMap = new ModelMap();
		
		orderInfo = orderDAO.getOrderById(orderInfo.getOrderNo());
		modelMap.addAttribute("orderInfo",orderInfo);
		modelMap.addAttribute("orderInfo",orderInfo);
		List<Tailor> lstTailor = tailorDAO.listTailor();
		modelMap.addAttribute("tailorLst",lstTailor);
		List<StitchType> lstStitchType = tailorDAO.listStitches();
		modelMap.addAttribute("stitchTypes",lstStitchType);
		modelMap.addAttribute("currFormat", "#,##,##,##0.00");
		modelMap.addAttribute("update","Update");
		Tailor tailor = null;
		try{
		tailor = tailorDAO.getTailorById(orderInfo.getTailorId());
		}catch(IndexOutOfBoundsException e){
			modelMap.addAttribute("error","The tailor assigned to this order was deleted. Please select another tailor and save the order.");
			modelMap.remove("calculate");			
			return new ModelAndView("addOrder", modelMap);
		}
		
		amtCalculation(orderInfo.getWindowLst(), lstStitchType, tailor, false);
		
		return new ModelAndView("calculateTailor", modelMap);
	}
	
	private void amtCalculation (Set<Window> wSet, List<StitchType> lstStitchType, Tailor tailor, boolean external)
	{
		for (Iterator iterator = wSet.iterator(); iterator.hasNext();) 
		{
			Window window = (Window) iterator.next();
			int stitchType = Integer.parseInt(window.getStyle())-1;
			String stitchTyp = lstStitchType.get(stitchType).getName();
			if(stitchTyp.equals("Eylet") || stitchTyp.equals("A/P") || stitchTyp.equals("Double"))	//EYLET or A/P or Double
			{
				window.setHeightMtrs(Math.ceil((double)(window.getHeight() + 12) * 2.0 / 39) * Long.parseLong(window.getPanels()) / 2.0);
								
				/*int panels = (int) Math.ceil((double)window.getWidth() / 20);
				System.out.println("Window Name:"+window.getName()+". No of panels:"+panels);*/
				
			}
			else if(stitchTyp.equals("Roman"))	//Roman
			{
				window.setHeightMtrs(Math.ceil((double)(window.getHeight() + 6) * 2.0 / 39)* Long.parseLong(window.getPanels()) / 2.0);
			}
			
			double amount = 0d;
			int index = external ? 0 : 1;
			
			if(stitchTyp.equals("Double"))
			{
				if(window.getDblTlrType().equals("Eylet"))
				{
					amount = Double.parseDouble(getTailorCharges(tailor, "1").split("::")[index]);		//1=Eylet
					amount += Double.parseDouble(getTailorCharges(tailor, "2").split("::")[index]);	//2=A/P
				}
				else if(window.getDblTlrType().equals("A/P"))
				{
					amount = Double.parseDouble(getTailorCharges(tailor, "2").split("::")[index]) * 2.0;
				}
				
				window.setTailorCharges((double) (Long.parseLong(window.getPanels()) * amount));
			}
			else if(stitchTyp.equals("Roman"))
			{
				amount = Double.parseDouble(getTailorCharges(tailor, window.getStyle()).split("::")[index]);
				double heightFt = Math.ceil((window.getHeight() / 12.0) * 2.0) / 2.0 ;
				double widthFt = Math.ceil((window.getWidth() / 12.0) * 2.0) / 2.0 ;
								
				window.setTailorCharges((double) heightFt * widthFt * amount);
			}
			else
			{
				amount = Double.parseDouble(getTailorCharges(tailor, window.getStyle()).split("::")[index]);
				window.setTailorCharges((double) (Long.parseLong(window.getPanels()) * amount));
			}						
		}
	}
	
	private String getTailorCharges(Tailor tailor, String windowStyle)
	{
		Set<TailorStitchType> tlrStitchTypes = tailor.getTlrStitchTyp();
		
		for (Iterator iterator2 = tlrStitchTypes.iterator(); iterator2.hasNext();) 
		{
			TailorStitchType tailorStitchType = (TailorStitchType) iterator2.next();
			TailorStitchId tailorStitchId = tailorStitchType.getPk();
			if(tailorStitchId.getStitchType().getTypeId() == Long.parseLong(windowStyle) && 
					tailorStitchId.getTailor().getTailorId() == tailor.getTailorId())
			{
				//System.out.println("window.name:"+window.getName()+" window.style:"+window.getStyle()+" amount:"+tailorStitchType.getAmount());
				return tailorStitchType.getAmount();
			}
		}
		
		return "";
	}
	
	public ModelAndView completeOrder(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		String orderNo = request.getParameter("orderNo");
		Double totalCharges = Double.parseDouble(request.getParameter("totalCharges"));
		OrderInfo orderInfo = orderDAO.getOrderById(orderNo);
		orderInfo.setTotalCharges(totalCharges);
		orderInfo.setStatus("COMPLETED");
		
		orderDAO.saveOrUpdateOrder(orderInfo);
		
		return calculate(request, response, orderInfo);
	}
	
	public ModelAndView search(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		String searchCriteria = request.getParameter("searchCrit");
		String searchFld = request.getParameter("searchFld");
		List<OrderInfo> lstOrders = null;
		if(searchCriteria.equals("Order Number"))
		{
			lstOrders = orderDAO.searchOrders(searchFld);
		}
		else if(searchCriteria.equals("Customer Name"))
		{
			lstOrders = orderDAO.searchByCustomer(searchFld);
		}
		PagedListHolder pagedListHolder = new PagedListHolder(lstOrders);
		pagedListHolder.setPageSize(20);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);  
        pagedListHolder.setPage(page);
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("pagedListHolder", pagedListHolder);
		modelMap.addAttribute("orderSize", lstOrders.size());				
		
		//modelMap.addAttribute("lstOrders", lstOrders);
		modelMap.addAttribute("searchFld", searchFld);
		modelMap.addAttribute("searchCrit", searchCriteria);
		
		return new ModelAndView("searchOrder", modelMap);
	}
	
	public ModelAndView reopenOrder(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		String orderNo = request.getParameter("orderNo");
		String status = request.getParameter("status");
		
		OrderInfo orderInfo = orderDAO.getOrderById(orderNo);
		orderInfo.setStatus(status);
		orderInfo.setTotalCharges(null);
		orderDAO.saveOrUpdateOrder(orderInfo);
		
		return updateOrder(request, response);
	}
	
	
	/*private String dateFormatter(String date)
	{
		if(date == null || date.equals(""))
			return "";
		
		StringTokenizer st = new StringTokenizer(date, "-");
		String dt = st.nextToken();
		int m = Integer.parseInt(st.nextToken());
		String yr = st.nextToken();
		String mon = "";
		switch (m) {
		case 1:
			mon = "Jan";
			break;
		case 2:
			mon = "Feb";
			break;
		case 3:
			mon = "Mar";
			break;
		case 4:
			mon = "Apr";
			break;
		case 5:
			mon = "May";
			break;
		case 6:
			mon = "Jun";
			break;
		case 7:
			mon = "Jul";
			break;
		case 8:
			mon = "Aug";
			break;
		case 9:
			mon = "Sep";
			break;
		case 10:
			mon = "Oct";
			break;
		case 11:
			mon = "Nov";
			break;
		case 12:
			mon = "Dec";
			break;

		default:
			break;
		}
		
		return dt+"-"+mon+"-"+yr;
	}*/

}