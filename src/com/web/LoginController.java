package com.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.UserDAO;
import com.domain.Config;
import com.domain.User;
import com.helper.Encryption;
import com.helper.SendEmail;
import com.helper.TempPasswordGenerator;

public class LoginController extends MultiActionController 
{
	private UserDAO userDAO;
	private static Config config;
	
	static
	{
		config = (Config)new ClassPathXmlApplicationContext("config.xml").getBean("config");
	}
	
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public ModelAndView doLogin(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception 
	{		
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("login", user);
						
		String strAdminId = config.getAdminUser();//PropertiesLoader.getProperty("adminempId");
		String adminPass = config.getAdminPass();//PropertiesLoader.getProperty("adminPass");
				
		String encPwd = Encryption.encrypt(user.getPassword());
		
		int status = userDAO.login(user.getUsername(), encPwd);
		if(status==0)
			user = userDAO.getUserByUsrNm(user.getUsername());
				
				
		if(!userDAO.checkAdminUserExist() && strAdminId.equalsIgnoreCase(user.getUsername()) && adminPass.equals(user.getPassword()))
		{
			System.out.println("Admin has logged in..");
			status = 0;
			user.setName("Admin");
			user.setIsAdmin(true);
		}	
		
		System.out.println("Status:"+status);
		if(status == 0)
		{			
			HttpSession session = request.getSession();
			session.setAttribute("login", user.getName());
			if(user.getIsAdmin()!=null)
			{
				session.setAttribute("admin", ""+user.getIsAdmin());
				System.out.println("Admin:"+user.getIsAdmin());
			}
			
			session.setAttribute("username", user.getUsername());
			session.setAttribute("location", request.getParameter("location"));
			
			return new ModelAndView("redirect:../order/addOrder.htm", modelMap);
		}
		else if(status == 1)
		{
			return new ModelAndView("resetPassword", modelMap);
		}
		else
		{
			modelMap.addAttribute("login", new User());
			modelMap.addAttribute("error", "Login Failed");
			return new ModelAndView("login", modelMap);
		}
	}
	
	public ModelAndView start(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception 
	{		
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("login", user);
		modelMap.addAttribute("location", config.getLocation());
		return new ModelAndView("login", modelMap);
	}
	
	public ModelAndView logout(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		
		String username = (String) request.getSession().getAttribute("username");
		User user = userDAO.getUserByUsrNm(username);
		
		if(user==null)
		{
			request.getSession().invalidate();
			return start(request, response, new User());
		}			
		
		//userDAO.updateUser(user);
		//modelMap.addAttribute("login", user);
		//request.getSession().removeAttribute("login");
		request.getSession().invalidate();
		return start(request, response, new User());
	}
	
	public ModelAndView resetPwd(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("login", user);
		User loadUser = userDAO.getUserByUsrNm(user.getUsername());
		String tmpPwd = loadUser.getPassword();
		String decPwd = Encryption.decrypt(tmpPwd);
	
		if(user.getOldPassword().equals(user.getPassword()))
		{
			modelMap.addAttribute("error", "Both passwords cannot be the same.");
			return new ModelAndView("resetPassword", modelMap);
		}
		else if(user.getOldPassword().equals(decPwd))	//Correct temporary password & not same
		{
			userDAO.savePassword(user.getUsername(), Encryption.encrypt(user.getPassword()), false);
			modelMap.addAttribute("location", config.getLocation());
			return new ModelAndView("login", modelMap);
		}
		
		modelMap.addAttribute("error", "Password is not correct.");
		return new ModelAndView("resetPassword", modelMap);
	}
	
	public ModelAndView forgot(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("login", user);
			
		return new ModelAndView("forgotPassword", modelMap);
	}
	
	public ModelAndView forgotPwd(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception 
	{
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("login", user);
		System.out.println("Sending password...");
		
		user = userDAO.getUserByUsrNm(user.getUsername());
		
		if(user==null)
		{
			modelMap.addAttribute("error", "We could not find that user !");
			return new ModelAndView("forgotPassword", modelMap);
		}
		
		//String[] to = {user.email};
		String password = TempPasswordGenerator.generatePwd();
		String body = "Hi "+user.getName()+",<br><br>Your password is: <b>"+password+"</b> <br>You will need to change it during login.<br><br>Thanks,<br>SoftZone Tool";
		
		userDAO.savePassword(user.getUsername(), Encryption.encrypt(password), true);
		try{
		SendEmail.sendEmail(new String[]{user.getEmail()}, "SoftZone Tool: Your temporary password", body, config);		
		}catch(Exception me)
		{
			String error = "Unable to send email to "+user.getEmail();
			modelMap.addAttribute("error", error);
		}
		modelMap.addAttribute("location", config.getLocation());
		return new ModelAndView("login", modelMap);
	}	

}