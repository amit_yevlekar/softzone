package com.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.UserDAO;
import com.domain.Config;
import com.domain.User;
import com.helper.Encryption;
import com.helper.SendEmail;
import com.helper.TempPasswordGenerator;

public class UserController extends MultiActionController {

	private UserDAO userDAO;
	
	private static Config config;
	
	static
	{
		config = (Config)new ClassPathXmlApplicationContext("config.xml").getBean("config");
	}
	
	private BindingResult errors;
	
	public BindingResult getErrors() {
		return errors;
	}

	public void setErrors(BindingResult errors) {
		this.errors = errors;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception 
	{
		//Validate user fields
		validateUserFields();
		
		try
		{			
			//save the temporary password
			String tempPwd = TempPasswordGenerator.generatePwd();
			user.setPassword(Encryption.encrypt(tempPwd));
			user.setResetInd(true);
			userDAO.saveUser(user);
			
			//send email of the password
			try{
			SendEmail.sendEmail(new String[]{user.getEmail()}, "SoftZone Tool: Your temporary password", "Hi "+user.getName()+",<br><br>Your temporary Password is: <b>"+tempPwd+"</b> <br><br>Thanks,<br>SoftZone Tool", config);
			}catch(Exception me)
			{
				String error = "Unable to send email to "+user.getEmail();				
				return new ModelAndView("redirect:user.htm?error="+error);
			}
			
		}
		catch (DataIntegrityViolationException e) 
		{
			System.out.println("The user is existing:"+e);
			String error = "The USERNAME ("+user.getUsername()+") is already taken";

			return new ModelAndView("redirect:user.htm?error="+error);
		}
		
		return new ModelAndView("redirect:user.htm");
	}
	
	private void validateUserFields() {
		
	}

	public ModelAndView delete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String [] values = request.getParameterValues("selectedUser");
		
		//List idList = new ArrayList();
		for (int j = 0; values!=null && j < values.length; j++) 
		{			
			userDAO.deleteUser(new User(values[j]));
		}
				
		return new ModelAndView("redirect:user.htm");
	}
	
	public ModelAndView update(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception {
		System.out.println("UserController:Update:" +user.getUsername());
		
		userDAO.updateUser(user);
		
		return new ModelAndView("redirect:user.htm");
	}

	public ModelAndView user(HttpServletRequest request,
			HttpServletResponse response, User user) throws Exception {
		ModelMap modelMap = new ModelMap();
		
		if(request.getParameter("error")!=null)
		{
			modelMap.addAttribute("error", request.getParameter("error"));
		}
		
		modelMap.addAttribute("userList", userDAO.listUser());
		//modelMap.addAttribute("hierUsers", userDAO.createHierarchy("0", new ArrayList(), " "));
		modelMap.addAttribute("user", user);
		return new ModelAndView("users", modelMap);
	}
	

}