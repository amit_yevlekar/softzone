package com.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.InventoryDAO;
import com.dao.SupplierDAO;
import com.domain.Customer;
import com.domain.Inventory;
import com.domain.OrderInfo;
import com.domain.Supplier;

public class InventoryController extends MultiActionController {

	private InventoryDAO inventoryDAO;
	private SupplierDAO suppDAO;
		
	private BindingResult errors;
	
	public BindingResult getErrors() {
		return errors;
	}

	public void setErrors(BindingResult errors) {
		this.errors = errors;
	}

	public void setInventoryDAO(InventoryDAO inventoryDAO) {
		this.inventoryDAO = inventoryDAO;
	}
	
	public void setSuppDAO(SupplierDAO suppDAO) {
		this.suppDAO = suppDAO;
	}
	
	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Inventory inventory) throws Exception 
	{
		//Validate Inventory fields
		validateInventoryFields();
		
		try
		{
			inventoryDAO.saveInventory(inventory);			
		}
		catch (DataIntegrityViolationException e) 
		{
			System.out.println("The Inventory is existing:"+e);
			errors.addError(new ObjectError("IntegrityViolation", "The Inventory is already taken"));
			return new ModelAndView("redirect:inventory.htm", errors.getModel());
		}
		
		return new ModelAndView("redirect:inventory.htm");
	}
	
	private void validateInventoryFields() {
		
	}

	public ModelAndView delete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String [] values = request.getParameterValues("selectedInventory");
		
		//List idList = new ArrayList();
		for (int j = 0; values!=null && j < values.length; j++) 
		{			
			inventoryDAO.deleteInventory(new Inventory(Long.parseLong(values[j])));
		}
				
		return new ModelAndView("redirect:inventory.htm");
	}
	
	public ModelAndView update(HttpServletRequest request,
			HttpServletResponse response, Inventory inventory) throws Exception {
		
		inventoryDAO.updateInventory(inventory);
		
		return new ModelAndView("redirect:inventory.htm");
	}

	public ModelAndView inventory(HttpServletRequest request,
			HttpServletResponse response, Inventory inventory) throws Exception {
		ModelMap modelMap = new ModelMap();
		List<Inventory> lstInventory = inventoryDAO.listInventory();
		//modelMap.addAttribute("inventoryList", lstInventory);
		modelMap.addAttribute("inventory", inventory);
		
		PagedListHolder pagedListHolder = new PagedListHolder(lstInventory);
		pagedListHolder.setPageSize(20);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);  
        pagedListHolder.setPage(page);
        modelMap.addAttribute("pagedListHolder", pagedListHolder);
		modelMap.addAttribute("inventorySize", lstInventory.size());
		
		modelMap.addAttribute("suppInfo",loadSupplierInfo());
		
		return new ModelAndView("inventory", modelMap);
	}
	
	private String loadSupplierInfo()
	{
		List<Supplier> suppliers = suppDAO.listSupplier();
		StringBuilder sbData = new StringBuilder();
		for (int i = 0; i < suppliers.size(); i++) 
		{
			sbData.append("{ value: \"")
				.append((suppliers.get(i)).getSuppName())
				.append("\",label: \"")
				.append((suppliers.get(i)).getSuppName())
				.append("\", id: \"").append((suppliers.get(i)).getSuppId());
			
			if(i+1 != suppliers.size())
				sbData.append("\"},");
			else
				sbData.append("\"}");
		}
		
		return sbData.toString();
	}
	
	public ModelAndView search(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		String searchCriteria = request.getParameter("searchCrit");
		String searchFld = request.getParameter("searchFld");
		List<Inventory> lstInventory = null;
		if(searchCriteria.equals("Collection Name"))
		{
			lstInventory = inventoryDAO.searchInventoryByCollectNm(searchFld);
		}
		else if(searchCriteria.equals("Serial No"))
		{
			lstInventory = inventoryDAO.searchInventoryBySerialNo(searchFld);
		}
		PagedListHolder pagedListHolder = new PagedListHolder(lstInventory);
		pagedListHolder.setPageSize(20);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);  
        pagedListHolder.setPage(page);
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("pagedListHolder", pagedListHolder);
		modelMap.addAttribute("inventorySize", lstInventory.size());
		
		modelMap.addAttribute("searchFld", searchFld);
		modelMap.addAttribute("searchCrit", searchCriteria);
		
		modelMap.addAttribute("suppInfo",loadSupplierInfo());
		modelMap.addAttribute("inventory", new Inventory());
		
		return new ModelAndView("inventory", modelMap);
	}
}