package com.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.TailorDAO;
import com.domain.Config;
import com.domain.StitchType;
import com.domain.Tailor;
import com.domain.TailorStitchId;
import com.domain.TailorStitchType;

public class TailorController extends MultiActionController {

	private TailorDAO tailorDAO;
	
	public void setTailorDAO(TailorDAO tailorDAO) {
		this.tailorDAO = tailorDAO;
	}
	
	public ModelAndView tailor(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		List<Tailor> lstTailor = tailorDAO.listTailor();
		modelMap.addAttribute("tailorList", lstTailor);
		//HashMap<Tailor, Long> stitchDetailsMap = new HashMap<Tailor, Long>();
		for (int i = 0; i < lstTailor.size(); i++) 
		{
			Tailor tailor = (Tailor)lstTailor.get(i);
			Set<TailorStitchType> set = tailor.getTlrStitchTyp();
						
			for (Iterator iterator = set.iterator(); iterator.hasNext();) {
				TailorStitchType tailorStitchType = (TailorStitchType) iterator.next();
				//System.out.println("set:"+tailorStitchType);				
				//tailorStitchType.getStitchType().getTypeId()
				//tailorStitchType.getAmount()
			}
		}
		
		//Check the Stitch types in config & in DB
		Config config = (Config)new ClassPathXmlApplicationContext("config.xml").getBean("config");
		int stitchTypesLength = config.getStitchId().split(",").length;		
		List<StitchType> lstStitchTypes = tailorDAO.listStitches();
		if(lstStitchTypes == null || lstStitchTypes.size() != stitchTypesLength)	// Load the stitch Types to DB
		{
			System.out.println("LOADING Stitch Types in DB");
			
			//Deleting existing entries
			//tailorDAO.deleteAllStitchTypes();
			
			String[] stitchIds = config.getStitchId().split(",");
			String[] stitchTypes = config.getStitchType().split(",");
			
			for (int i = 0; i < stitchIds.length; i++) 
			{
				StitchType stitchType = new StitchType(Long.parseLong(stitchIds[i]), stitchTypes[i]);
				tailorDAO.addStitchType(stitchType);
			}
			
			lstStitchTypes = tailorDAO.listStitches();
			
		}
		modelMap.addAttribute("lstStitchTypes", lstStitchTypes);
		modelMap.addAttribute("tailor", new Tailor());
		return new ModelAndView("tailor", modelMap);
	}
	
	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Tailor tailor) throws Exception
	{
		ModelMap modelMap = new ModelMap();
						
		String[] stitchTypeId = request.getParameterValues("stitchTypeId");
		String[] stitchTypeAmount = request.getParameterValues("stitchTypeAmount");
		String[] stitchTypeAmountInternal = request.getParameterValues("stitchTypeAmountInternal");
		
		for (int i = 0; i < stitchTypeId.length; i++) 
		{
			Long stitchId = Long.parseLong(stitchTypeId[i]);
			StitchType stitchType = tailorDAO.getStitchTypeById(stitchId);
			
			TailorStitchType tailorStitchType = new TailorStitchType();
			tailorStitchType.setTailor(tailor);
			tailorStitchType.setStitchType(stitchType);
			tailorStitchType.setAmount(stitchTypeAmount[i]+"::"+stitchTypeAmountInternal[i]);
			
			tailor.getTlrStitchTyp().add(tailorStitchType);			
		}
		
		tailorDAO.addTailor(tailor);
		
		return new ModelAndView("redirect:tailor.htm");
	}
	
	public ModelAndView delete(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ModelMap modelMap = new ModelMap();
		String [] values = request.getParameterValues("selectedTailor");
		
		//List idList = new ArrayList();
		for (int j = 0; values!=null && j < values.length; j++) 
		{
			Long tailorId = Long.parseLong(values[j]);
			
			Tailor tailor = tailorDAO.getTailorById(tailorId);			
			tailorDAO.deleteTailor(tailor);
		}
		
		return new ModelAndView("redirect:tailor.htm");
	}
	
	public ModelAndView update(HttpServletRequest request,
			HttpServletResponse response, Tailor tailor) throws Exception {
		System.out.println("TailorController:Update:" +tailor);
		
		String[] stitchTypeId = request.getParameterValues("stitchTypeId");
		String[] stitchTypeAmount = request.getParameterValues("stitchTypeAmount");
		String[] stitchTypeAmountInternal = request.getParameterValues("stitchTypeAmountInternal");
		
		for (int i = 0; i < stitchTypeId.length; i++) 
		{
			Long stitchId = Long.parseLong(stitchTypeId[i]);
			StitchType stitchType = tailorDAO.getStitchTypeById(stitchId);
			
			TailorStitchType tailorStitchType = new TailorStitchType();
			tailorStitchType.setTailor(tailor);
			tailorStitchType.setStitchType(stitchType);
			tailorStitchType.setAmount(stitchTypeAmount[i]+"::"+stitchTypeAmountInternal[i]);
			
			tailor.getTlrStitchTyp().add(tailorStitchType);
			
		}
		
		tailorDAO.updateTailor(tailor);
		
		return new ModelAndView("redirect:tailor.htm");
	}
	
	public ModelAndView payTailor(HttpServletRequest request,
			HttpServletResponse response) throws Exception 
	{
		Long tailorId = Long.parseLong(request.getParameter("id"));
		Tailor tailor = tailorDAO.getTailorById(tailorId);
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("tailor", tailor);
		
		return new ModelAndView("tailorPay", modelMap);
	}
	
	public ModelAndView pay(HttpServletRequest request,
			HttpServletResponse response, Tailor tailor) throws Exception 
	{
		tailor = tailorDAO.getTailorById(tailor.getTailorId());
		Long amt = Long.parseLong(request.getParameter("amt"));
		
		tailor.setPaidAmount(tailor.getPaidAmount()+amt);
		tailor.setOutstandingBal(tailor.getOutstandingBal()-amt);
		tailorDAO.updateTailor(tailor);
		
		return new ModelAndView("redirect:tailor.htm");
	}

}