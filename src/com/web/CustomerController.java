package com.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.dao.CustomerDAO;
import com.domain.Customer;

public class CustomerController extends MultiActionController {

	private CustomerDAO custDAO;
	
	private BindingResult errors;
	
	public BindingResult getErrors() {
		return errors;
	}

	public void setErrors(BindingResult errors) {
		this.errors = errors;
	}

	public void setCustDAO(CustomerDAO custDAO) {
		this.custDAO = custDAO;
	}
	
	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Customer customer) throws Exception 
	{		
		custDAO.saveCustomer(customer);		
		
		return new ModelAndView("redirect:customer.htm");
	}
	
	public ModelAndView delete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String [] values = request.getParameterValues("selectedCust");
		
		//List idList = new ArrayList();
		for (int j = 0; values!=null && j < values.length; j++) 
		{			
			custDAO.deleteCustomer(new Customer(Long.parseLong(values[j])));
		}
				
		return new ModelAndView("redirect:customer.htm");
	}
	
	public ModelAndView update(HttpServletRequest request,
			HttpServletResponse response, Customer customer) throws Exception {
		
		custDAO.updateCustomer(customer);
		
		return new ModelAndView("redirect:customer.htm");
	}

	public ModelAndView customer(HttpServletRequest request,
			HttpServletResponse response, Customer customer) throws Exception {
		ModelMap modelMap = new ModelMap();
		
		if(request.getParameter("error")!=null)
		{
			modelMap.addAttribute("error", request.getParameter("error"));
		}
		
		modelMap.addAttribute("custList", custDAO.listCustomer());
		
		modelMap.addAttribute("customer", customer);
		return new ModelAndView("customer", modelMap);
	}
	

}