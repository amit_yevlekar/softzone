package com.dao;


import java.util.List;

import com.domain.Inventory;
import com.domain.OrderInfo;

public interface InventoryDAO 
{	
	public void saveInventory(Inventory inventory) ;
	public List<Inventory> listInventory() ;
	public void deleteInventory(Inventory inventory);
	public void updateInventory(Inventory inventory);
	public List<Inventory> searchInventoryByCollectNm(String collNm);
	public List<Inventory> searchInventoryBySerialNo(String serialNo);
}
