package com.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.domain.Customer;
import com.domain.User;

public class CustomerDAOImpl implements CustomerDAO 
{

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveCustomer(Customer customer) 
	{
		hibernateTemplate.save(customer);	
	}
	
	@Override
	public List<Customer> listCustomer() {
		return hibernateTemplate.find("from Customer");
	}

	@Override
	public void deleteCustomer(Customer customer) 
	{
		hibernateTemplate.delete(customer);
	}
	
	@Override
	public User getCustomerByName(String custName) {
		ArrayList<User> temp = (ArrayList<User>)hibernateTemplate.find("from Customer where custName='"+custName+"'");
		if(temp !=null && temp.size() > 0)
		{			
			return temp.get(0);
		}
		
		return null;
	}
	
	
	@Override
	public void updateCustomer(Customer customer) 
	{

		hibernateTemplate.update(customer);
		
	}
	
	

}
