package com.dao;

import java.util.List;

import com.domain.StitchType;
import com.domain.Tailor;



public interface TailorDAO 
{	
	public List<Tailor> listTailor();
	public void addTailor(Tailor tailor);
	public void deleteTailor(Tailor tailor);
	public void updateTailor(Tailor tailor);
	public Tailor getTailorById(Long tailorId);
	public List<StitchType> listStitches();
	public void addStitchType(StitchType stitchType);
	public void deleteAllStitchTypes();
	public StitchType getStitchTypeById(Long id);
}
