package com.dao;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.domain.User;

public class UserDAOImpl implements UserDAO 
{

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveUser(User user) 
	{		
		hibernateTemplate.save(user);		
	}
	
	@Override
	public List<User> listUser() 
	{
		return hibernateTemplate.find("from User");
	}

	@Override
	public void deleteUser(User user) 
	{
		//user = getUserByUsrNm(user.getUsername());		
		
		hibernateTemplate.delete(user);
			
	}
	
	@Override
	public User getUserByUsrNm(String username)
	{
		
		ArrayList<User> temp = (ArrayList<User>)hibernateTemplate.find("from User where username='"+username+"'");
		if(temp !=null && temp.size() > 0)
		{			
			return temp.get(0);
		}
		
		return null;
	}
	
	@Override
	public void updateUser(User user) 
	{
		//user.password = Encryption.encrypt(user.password);
		System.out.println(user);
		hibernateTemplate.update(user);
		
	}
	
	@Override
	public int login(String usr, String pwd) 
	{
		ArrayList<User> temp = (ArrayList<User>)hibernateTemplate.find("from User where username='"+usr+"'");
		
		if(temp !=null && temp.size() > 0)
		{
			User user = temp.get(0);
			if(pwd.equals(user.getPassword()))
			{
				if(user.isResetInd() == true)
					return 1;
				return 0;
			}
			else
				return -2;
		}
		else
		{
			return -1;
		}		
	}
	
	
	@Override
	public void savePassword(String username, String pwd, boolean resetInd) 
	{
		
		hibernateTemplate.bulkUpdate("Update User user " +
				" set user.password = '" +pwd+"' , user.resetInd = " +resetInd+
				" where user.username = '"+username+"'");
		
	}
	
	@Override
	public boolean checkAdminUserExist() 
	{
		HibernateCallback callback = new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Long count = (Long) session.createQuery("select count(*) from User where isAdmin=true").uniqueResult();
				if(count>0l)
					return true;
				else
					return false;
			}
		};
		
		return (boolean) hibernateTemplate.execute(callback);
		
		//Session session = hibernateTemplate.getSessionFactory().openSession();		
		//Long count = (Long) session.createQuery("select count(*) from User where isAdmin=true").uniqueResult();
		//session.clear();
				
	}

}
