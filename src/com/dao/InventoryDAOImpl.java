package com.dao;


import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.domain.Inventory;
import com.domain.OrderInfo;

public class InventoryDAOImpl implements InventoryDAO 
{

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveInventory(Inventory inventory) 
	{		
		hibernateTemplate.save(inventory);		
	}
	
	@Override
	public List<Inventory> listInventory() 
	{
		return hibernateTemplate.find("from Inventory");
	}

	@Override
	public void deleteInventory(Inventory inventory) 
	{		
		hibernateTemplate.delete(inventory);
	}
	
	@Override
	public void updateInventory(Inventory inventory) 
	{
		hibernateTemplate.update(inventory);
		
	}
	
	@Override
	public List<Inventory> searchInventoryByCollectNm(String collNm) 
	{
		return hibernateTemplate.find("from Inventory where upper(collectName) like '%"+collNm.toUpperCase()+"%'");
	}
	
	@Override
	public List<Inventory> searchInventoryBySerialNo(String serialNo) 
	{
		return hibernateTemplate.find("from Inventory where upper(serialNo) like '%"+serialNo.toUpperCase()+"%'");
	}

}
