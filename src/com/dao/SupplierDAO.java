package com.dao;


import java.util.List;

import com.domain.Supplier;

public interface SupplierDAO 
{	
	public void saveSupplier(Supplier supplier) ;
	public List<Supplier> listSupplier() ;
	public void deleteSupplier(Supplier supplier);
	public void updateSupplier(Supplier supplier);
	
}
