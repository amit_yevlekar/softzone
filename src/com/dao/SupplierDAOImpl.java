package com.dao;


import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.domain.Supplier;

public class SupplierDAOImpl implements SupplierDAO 
{

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveSupplier(Supplier supplier) 
	{		
		hibernateTemplate.save(supplier);		
	}
	
	@Override
	public List<Supplier> listSupplier() 
	{
		return hibernateTemplate.find("from Supplier");
	}

	@Override
	public void deleteSupplier(Supplier supplier) 
	{		
		hibernateTemplate.delete(supplier);
	}
	
	@Override
	public void updateSupplier(Supplier supplier) 
	{
		System.out.println(supplier);
		hibernateTemplate.update(supplier);
		
	}

}
