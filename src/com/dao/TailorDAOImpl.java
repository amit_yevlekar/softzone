package com.dao;


import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.domain.StitchType;
import com.domain.Tailor;

public class TailorDAOImpl implements TailorDAO {

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public List<Tailor> listTailor() 
	{
		return hibernateTemplate.find("from Tailor");
	
	}
	
	@Override
	public void addTailor(Tailor tailor) 
	{
		hibernateTemplate.save(tailor);
	}
	
	@Override
	public void deleteTailor(Tailor tailor) 
	{
		hibernateTemplate.delete(tailor);
	}
	
	@Override
	public void updateTailor(Tailor tailor) 
	{
		hibernateTemplate.update(tailor);		
	}
	
	@Override
	public Tailor getTailorById(Long tailorId) 
	{
		Tailor tailor = (Tailor)hibernateTemplate.find("from Tailor where tailorId="+tailorId).get(0);
		return tailor;
	}
	
	@Override
	public List<StitchType> listStitches() {
		return hibernateTemplate.find("from StitchType order by typeId");
	}
	
	@Override
	public void addStitchType(StitchType stitchType) 
	{
		List stitchLst = hibernateTemplate.find("from StitchType where typeId="+stitchType.getTypeId());
		if(stitchLst.size() == 0)
			hibernateTemplate.save(stitchType);
		else
			hibernateTemplate.update(stitchType);		
	}
	
	@Override
	public void deleteAllStitchTypes() 
	{
		hibernateTemplate.deleteAll(hibernateTemplate.find("from StitchType"));
		
	}
	
	@Override
	public StitchType getStitchTypeById(Long typeId) {
		return (StitchType)hibernateTemplate.find("from StitchType where typeId="+typeId).get(0);
	}

}
