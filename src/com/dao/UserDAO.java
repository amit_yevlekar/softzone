package com.dao;


import java.util.List;

import com.domain.User;

public interface UserDAO 
{	
	public void saveUser(User user) ;
	public List<User> listUser() ;
	public void deleteUser(User user);
	public User getUserByUsrNm(String username);
	public void updateUser(User user);
	
	public int login(String usr, String pwd);
	public boolean checkAdminUserExist();
	
	public void savePassword(String username, String pwd, boolean resetInd);
	
	
}
