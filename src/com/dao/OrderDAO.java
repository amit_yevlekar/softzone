package com.dao;

import java.util.List;

import com.domain.OrderInfo;
import com.domain.Window;


public interface OrderDAO 
{	
	public String generateId();
	public OrderInfo getOrderById(String orderNo);
	public Window getWindowById(Long windowId);
	public void saveOrUpdateOrder(OrderInfo orderInfo);
	public void saveOrUpdateWindow(Window window);
	public List<OrderInfo> lstOrders();
	public Window mergeWindow(Window window);
	public void deleteWindow(Window window);
	public void deleteOrder(OrderInfo orderInfo);
	public List<OrderInfo> searchOrders(String orderStr);
	public List<OrderInfo> searchByCustomer(String custNm);
	public List<String[]> getTurnOver(String year);
	public List<String[]> getOrderStatus(String year);
	public String getOrderYears();
	//public void changeOrderStatus(String orderNo, String status);
	
}
