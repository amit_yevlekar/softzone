package com.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.util.StringUtils;

import com.domain.OrderInfo;
import com.domain.Window;

public class OrderDAOImpl implements OrderDAO {

	private HibernateTemplate hibernateTemplate;
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}


	public String generateId()
	{
		String yr_mn = new SimpleDateFormat("yMM").format(new Date());
		System.out.println(yr_mn);
		
		final String sql ="SELECT substr(max(ORDER_NO), 7) as MAXCNT FROM ORDER_INFO where ORDER_NO like '"+yr_mn+"%' ";
		
		 Object cnt = hibernateTemplate.execute(
		   new HibernateCallback() {
			    public Object doInHibernate(Session session) throws HibernateException {
		     SQLQuery query = session.createSQLQuery(sql);
		     return  query.uniqueResult();
		    }});
		 
		if(cnt==null)
			return (yr_mn+String.format("%04d", 1));
		
		return (yr_mn+String.format("%04d", (Integer.parseInt((String)cnt)+1)));
	}
	
	@Override
	public OrderInfo getOrderById(String orderNo) 
	{
		List orderLst = hibernateTemplate.find("from OrderInfo where orderNo="+orderNo);
		if(orderLst.size() == 0)
			return null;
		
		OrderInfo orderInfo = (OrderInfo)orderLst.get(0);
		return orderInfo;
	}
	
	@Override
	public Window getWindowById(Long windowId) 
	{
		List windowLst = hibernateTemplate.find("from Window where windowId="+windowId);
		if(windowLst.size() == 0)
			return null;
		
		Window window = (Window)windowLst.get(0);
		return window;
	}
	
	@Override
	public void saveOrUpdateOrder(OrderInfo orderInfo) 
	{
		if(getOrderById(orderInfo.getOrderNo()) == null)
		{
			orderInfo.setCreationDate(new Date());
			hibernateTemplate.save(orderInfo);
		}
		else
		{
			orderInfo.setUpdateDate(new Date());
			hibernateTemplate.update(orderInfo);
		}
	}
	
	@Override
	public void saveOrUpdateWindow(Window window) 
	{
		if(getWindowById(window.getWindowId())== null)
			hibernateTemplate.save(window);
		else
			hibernateTemplate.update(window);
		
	}
	
	@Override
	public List<OrderInfo> lstOrders() {
		return hibernateTemplate.find("from OrderInfo order by orderNo desc");
	}
	
	@Override
	public Window mergeWindow(Window window) {
		return hibernateTemplate.merge(window);
	}
	
	@Override
	public void deleteWindow(Window window) {
		hibernateTemplate.delete(window);
	}
	
	@Override
	public void deleteOrder(OrderInfo orderInfo) {
		hibernateTemplate.delete(orderInfo);		
	}
	
	@Override
	public List<OrderInfo> searchOrders(String orderStr) 
	{
		return hibernateTemplate.find("from OrderInfo where orderNo like '"+orderStr+"%'");
	}
	
	@Override
	public List<OrderInfo> searchByCustomer(String custNm) 
	{
		return hibernateTemplate.find("from OrderInfo where upper(customerName) like '%"+custNm.toUpperCase()+"%'");
	}
	
	@Override
	public List<String[]> getTurnOver(String year) {
		String query = "select substring(ORDER_NO,1,4) ordSub, sum(tot_chrg) totChrg from ORDER_INFO where STATUS='COMPLETED' and substring(ORDER_NO,1,2) = ? group by substring(ORDER_NO,1,4) order by ORDER_NO";
		Object[] queryParam = {year.substring(2)};
		RowMapper mapper = new RowMapper() {
            public Object[] mapRow(ResultSet rs, int rowNum) throws SQLException 
            {
            	String[] ret = new String[2];
                ret[0] = rs.getString("ordSub");
                ret[1] = rs.getString("totChrg");
                return ret;
            }
        };
        
		return (List<String[]>)jdbcTemplate.query(query, queryParam, mapper);
	}
	
	@Override
	public List<String[]> getOrderStatus(String year) {
		String query = "select substring(ORDER_NO,1,4) ordSub, status, count(status) cnt from ORDER_INFO where substring(ORDER_NO,1,2) = ? group by substring(ORDER_NO,1,4), status order by ORDER_NO";
		
		Object[] queryParam = {year.substring(2)};
		RowMapper mapper = new RowMapper() {
            public Object[] mapRow(ResultSet rs, int rowNum) throws SQLException 
            {
            	String[] ret = new String[3];
                ret[0] = rs.getString("ordSub");
                ret[1] = rs.getString("status");
                ret[2] = rs.getString("cnt");
                return ret;
            }
        };
        
		return (List<String[]>)jdbcTemplate.query(query, queryParam, mapper);
	}
	
	@Override
	public String getOrderYears() {
		String query = "select distinct substring(ORDER_NO,1,2) ordSub from ORDER_INFO order by 1 desc";
		
		RowMapper mapper = new RowMapper() {
            public Object[] mapRow(ResultSet rs, int rowNum) throws SQLException 
            {
            	String[] ret = new String[1];
                ret[0] = "20"+rs.getString("ordSub");

                return ret;
            }
        };
        
        List<Object[]> slist = (List<Object[]>)jdbcTemplate.query(query, mapper);
        String ret = "";
        for (int i = 0; i < slist.size(); i++) 
        {
			ret += slist.get(i)[0]+((i+1!=slist.size())?",":"");
		}
        
		return ret;
	}
	
	
	/*@Override
	public void changeOrderStatus(String orderNo, String status) {
		OrderInfo orderInfo = getOrderById(orderNo);
		orderInfo.setStatus(status);
		
		saveOrUpdateOrder(orderInfo);
	}*/

}
