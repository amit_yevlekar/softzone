package com.dao;


import java.util.List;

import com.domain.Customer;
import com.domain.User;

public interface CustomerDAO 
{	
	public void saveCustomer(Customer customer) ;
	public List<Customer> listCustomer() ;
	public void deleteCustomer(Customer customer);
	public User getCustomerByName(String custName);
	public void updateCustomer(Customer customer);
	
	
}
