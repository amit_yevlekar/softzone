package com.helper;


import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.domain.Config;


public class SendEmail {
	
			
	public static void sendEmail(String[] to, String subject, String body, Config config) throws Exception 
	{

		// Get system properties
		Properties props = System.getProperties();
				
		// Setup mail server
		//props.put("mail.smtp.host",config.getSmtpHost());
		
		props.put("mail.smtp.starttls.enable", "true"); // added this line
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", "true");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(props,new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        "softzone.app", "softzone123");
            }}
                );

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(config.getEmailFrom()));
			
			InternetAddress[] addressTo = new InternetAddress[to.length];
	        for (int i = 0; i < to.length; i++) {
	            addressTo[i] = new InternetAddress(to[i]);
	        }
			
			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, addressTo);

			// Set Subject: header field
			message.setSubject("SoftZone Tool: "+subject);
					
			message.setContent("<h3>"+body+"</h3>", "text/html");

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully-> User:'"+to[0]+"' Subject:"+subject);
		} catch (Exception mex) {
			mex.printStackTrace();
			throw mex;
		}
	}

	public static void sendFile(String[] to, String subject, String body, Config config) throws MessagingException 
	{
		
		// Get system properties
		Properties props = System.getProperties();

		// Setup mail server
		props.put("mail.smtp.starttls.enable", "true"); // added this line
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", "true");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(props,new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        "softzone.app", "softzone123");
            }}
                );

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			
			// Set From: header field of the header.
			message.setFrom(new InternetAddress("ADM"));

			InternetAddress[] addressTo = new InternetAddress[to.length];
	        for (int i = 0; i < to.length; i++) {
	            addressTo[i] = new InternetAddress(to[i]);
	        }
			
			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, addressTo);

			// Set Subject: header field
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText(body);

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			String filename = "C:/Users/Amit/Desktop/Amit Yevlekar.doc";
			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
			throw mex;
		}
	}

	public static void main(String[] args) throws Exception 
	{
		Config config = new Config();
		config.setSmtpHost("smtp.gmail.com");
		config.setEmailFrom("SoftZone@gmail.com");
		//sendEmail(new String[]{"amitye@amdocs.com"}, "Password reset", "Your new password is Y%rfj", config);
		sendEmail(new String[]{"amityevlekar@gmail.com"}, "Password reset", "Your new password is Y%rfj", config);
	}
}