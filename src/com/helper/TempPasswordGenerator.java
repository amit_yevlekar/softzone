package com.helper;

import java.security.SecureRandom;
import java.util.Random;

public class TempPasswordGenerator 
{
	private static final Random RANDOM = new SecureRandom();
	
	public static String generatePwd() {
		// Pick from some letters that won't be easily mistaken for each
	      // other. So, for example, omit o O and 0, 1 l and L.
	      String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";

	      String pw = "";
	      for (int i=0; i<5; i++)
	      {
	          int index = (int)(RANDOM.nextDouble()*letters.length());
	          pw += letters.substring(index, index+1);
	      }
	     
	      return pw;
	      
	}

}
