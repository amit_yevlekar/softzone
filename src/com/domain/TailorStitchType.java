package com.domain;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TAILOR_STITCHTYPE")
@AssociationOverrides({
	@AssociationOverride(name = "pk.tailor", 
		joinColumns = @JoinColumn(name = "TAILOR_ID")),
	@AssociationOverride(name = "pk.stitchType", 
		joinColumns = @JoinColumn(name = "TYPE_ID")) })
public class TailorStitchType 
{
	private TailorStitchId pk = new TailorStitchId();
	private String amount;
	
	@EmbeddedId
	public TailorStitchId getPk() {
		return pk;
	}
	public void setPk(TailorStitchId pk) {
		this.pk = pk;
	}
	
	@Transient
	public Tailor getTailor()
	{
		return getPk().getTailor();
	}
	public void setTailor(Tailor tailor)
	{
		getPk().setTailor(tailor);
	}
	
	@Transient
	public StitchType getStitchType()
	{
		return getPk().getStitchType();
	}
	public void setStitchType(StitchType stitchType)
	{
		getPk().setStitchType(stitchType);
	}
	
	@Column(name="AMOUNT")
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
