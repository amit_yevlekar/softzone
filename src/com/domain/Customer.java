package com.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="CUSTOMER")
public class Customer {
	private Long custId;
	private String custName;
	private String custMobile;
	private String custAddr;
	private String custEmail;
	
	public Customer() {	}
	
	public Customer(Long custId)
	{
		this.custId = custId;
	}	
	
	@Id
	@Column(name="CUSTOMER_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	@Column(name="CUSTOMER_NAME")
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	@Column(name="CUSTOMER_MOBILE")
	public String getCustMobile() {
		return custMobile;
	}

	public void setCustMobile(String custMobile) {
		this.custMobile = custMobile;
	}

	@Column(name="CUSTOMER_ADDRESS")
	public String getCustAddr() {
		return custAddr;
	}

	public void setCustAddr(String custAddr) {
		if(custAddr.indexOf("\n") != -1)
		{
			custAddr = custAddr.replaceAll("\r\n", "<br>");		
		}
		this.custAddr = custAddr;
	}

	@Column(name="CUSTOMER_EMAIL")
	public String getCustEmail() {
		return custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	

}
