package com.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SUPPLIER")
public class Supplier {

	private Long suppId;
	private String suppName;
	private String suppCode;
	private Double suppRate;
	private Long suppMobileNo;
	private String suppEmail;
	private String softzoneCode;
	
	public Supplier()
	{}
	
	public Supplier(Long suppId) {
		this.suppId = suppId;
	}
	@Id
	@Column(name="SUPP_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getSuppId() {
		return suppId;
	}
	public void setSuppId(Long suppId) {
		this.suppId = suppId;
	}
	
	@Column(name="SUPP_NAME")
	public String getSuppName() {
		return suppName;
	}
	public void setSuppName(String suppName) {
		this.suppName = suppName;
	}
	
	@Column(name="SUPP_CODE")
	public String getSuppCode() {
		return suppCode;
	}
	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}
	
	@Column(name="SUPP_RATE")
	public Double getSuppRate() {
		return suppRate;
	}
	public void setSuppRate(Double suppRate) {
		this.suppRate = suppRate;
	}
	
	@Column(name="SUPP_MOB_NO")
	public Long getSuppMobileNo() {
		return suppMobileNo;
	}
	public void setSuppMobileNo(Long suppMobileNo) {
		this.suppMobileNo = suppMobileNo;
	}
	
	@Column(name="SUPP_EMAIL")
	public String getSuppEmail() {
		return suppEmail;
	}
	
	public void setSuppEmail(String suppEmail) {
		this.suppEmail = suppEmail;
	}
	
	@Column(name="SOFT_CODE")
	public String getSoftzoneCode() {
		return softzoneCode;
	}
	public void setSoftzoneCode(String softzoneCode) {
		this.softzoneCode = softzoneCode;
	}
	
	
}
