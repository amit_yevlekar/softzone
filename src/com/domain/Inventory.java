package com.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="INVENTORY")
public class Inventory {

	private Long inventoryId;
	private String collectName;
	private String quality;
	private String shade;
	private String serialNo;
	private String suppId;
	private String suppRefName;
	private Double cntrRollRate;
	private Double cntrCutRate;
	private Double mrp;
	private Double stockMtrsLaxmi;
	private Double stockMtrsKondhwa;
		
	public Inventory()
	{}
	
	public Inventory(Long inventId) {
		this.inventoryId = inventId;
	}
	
	@Id
	@Column(name="INVENTORY_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getInventoryId() {
		return inventoryId;
	}
	
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}

	@Column(name="COLLECTION_NAME")
	public String getCollectName() {
		return collectName;
	}

	public void setCollectName(String collectName) {
		this.collectName = collectName;
	}

	@Column(name="QUALITY")
	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	@Column(name="SHADE")
	public String getShade() {
		return shade;
	}

	public void setShade(String shade) {
		this.shade = shade;
	}

	@Column(name="SERIAL_NO")
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name="SUPP_ID")
	public String getSuppId() {
		return suppId;
	}

	public void setSuppId(String suppId) {
		this.suppId = suppId;
	}

	@Column(name="SUPP_REF_NAME")
	public String getSuppRefName() {
		return suppRefName;
	}

	public void setSuppRefName(String suppRefName) {
		this.suppRefName = suppRefName;
	}

	@Column(name="COUNTER_ROLL_RATE")
	public Double getCntrRollRate() {
		return cntrRollRate;
	}

	public void setCntrRollRate(Double cntrRollRate) {
		this.cntrRollRate = cntrRollRate;
	}

	@Column(name="COUNTER_CUT_RATE")
	public Double getCntrCutRate() {
		return cntrCutRate;
	}

	public void setCntrCutRate(Double cntrCutRate) {
		this.cntrCutRate = cntrCutRate;
	}

	@Column(name="MRP")
	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	@Column(name="STOCK_MTRS_LAXMIRD")
	public Double getStockMtrsLaxmi() {
		return stockMtrsLaxmi;
	}

	public void setStockMtrsLaxmi(Double stockMtrsLaxmi) {
		this.stockMtrsLaxmi = stockMtrsLaxmi;
	}

	@Column(name="STOCK_MTRS_KONDHWA")
	public Double getStockMtrsKondhwa() {
		return stockMtrsKondhwa;
	}

	public void setStockMtrsKondhwa(Double stockMtrsKondhwa) {
		this.stockMtrsKondhwa = stockMtrsKondhwa;
	}

	
	
	
}
