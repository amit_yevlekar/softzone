package com.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TAILOR")
public class Tailor {

	private Long tailorId;
	private String name;
	private String mobNo;
	private String address;
	private double outstandingBal;
	private double paidAmount;
	private Set<TailorStitchType> tlrStitchTyp = new HashSet<TailorStitchType>(0);
	
	public Tailor() {}
	public Tailor(Long tailorId)
	{
		this.tailorId = tailorId;
	}
	
	@Id
	@Column(name="TAILOR_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getTailorId() {
		return tailorId;
	}
	public void setTailorId(Long tailorId) {
		this.tailorId = tailorId;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="MOBILE_NO")
	public String getMobNo() {
		return mobNo;
	}
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}
	
	@Column(name="ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		if(address.indexOf("\n") != -1)
		{
			address = address.replaceAll("\r\n", "<br>");		
		}
		this.address = address;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.tailor", cascade=CascadeType.ALL)
	public Set<TailorStitchType> getTlrStitchTyp() {
		return tlrStitchTyp;
	}
	public void setTlrStitchTyp(Set<TailorStitchType> tlrStitchTyp) {
		this.tlrStitchTyp = tlrStitchTyp;
	}
	
	@Column(name="OUT_BAL_AMT")
	public double getOutstandingBal() {
		return outstandingBal;
	}
	public void setOutstandingBal(double outstandingBal) {
		this.outstandingBal = outstandingBal;
	}
	
	@Column(name="PAID_AMT")
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	
}
