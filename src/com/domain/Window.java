package com.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="WINDOW")
public class Window
{
	private Long windowId;
	private String name;
	private Long width;
	private Long height;
	private String style;
	private Double price;
	private Double dblPrice;
	private String info;
	private OrderInfo orderInfo;
	private String panels;
	private Boolean customPanels;
	private Double heightMtrs;
	private Double tailorCharges;
	private String dblTlrType;
	
	public Window() {}
	public Window(Long windowId)
	{
		this.windowId = windowId;
	}
	
	@Id
	@Column(name="WINDOW_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getWindowId() {
		return windowId;
	}
	public void setWindowId(Long windowId) {
		this.windowId = windowId;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="WIDTH")
	public Long getWidth() {
		return width;
	}
	public void setWidth(Long width) {
		this.width = width;
	}
	
	@Column(name="HEIGHT")
	public Long getHeight() {
		return height;
	}
	public void setHeight(Long height) {
		this.height = height;
	}
	
	@Column(name="STYLE")
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	
	@Column(name="PRICE")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name="DBL_PRICE")
	public Double getDblPrice() {
		return dblPrice;
	}
	public void setDblPrice(Double dblPrice) {
		this.dblPrice = dblPrice;
	}
	
	@Column(name="INFO")
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "ORDER_NO", nullable = false)
	public OrderInfo getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}
	
	@Column(name="PANELS")
	public String getPanels() {
		return panels;
	}
	public void setPanels(String panels) {
		this.panels = panels;
	}
	
	@Column(name="CUST_PANELS")
	public Boolean getCustomPanels() {
		return customPanels;
	}
	public void setCustomPanels(Boolean customPanels) {
		this.customPanels = customPanels;
	}
	
	@Column(name="DBL_TLR_TYP")
	public String getDblTlrType() {
		return dblTlrType;
	}	
	public void setDblTlrType(String dblTlrType) {
		this.dblTlrType = dblTlrType;
	}
	
	@Transient
	public Double getHeightMtrs() {
		return heightMtrs;
	}
	public void setHeightMtrs(Double heightMtrs) {
		this.heightMtrs = heightMtrs;
	}
	
	@Transient
	public Double getTailorCharges() {
		return tailorCharges;
	}
	public void setTailorCharges(Double tailorCharges) {
		this.tailorCharges = tailorCharges;
	}
	
}
