package com.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

//Eylet, A/M, Roman

@Entity
@Table(name="STITCHTYPE")
public class StitchType {

	private Long typeId;
	private String name;
	private Set<TailorStitchType> tlrStitchTyp = new HashSet<TailorStitchType>(0);
	
	public StitchType() {
	}
	
	public StitchType(Long typeId, String name)
	{
		this.typeId = typeId;
		this.name = name;
	}
	
	@Id
	@Column(name="TYPE_ID")
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.stitchType")
	public Set<TailorStitchType> getTlrStitchTyp() {
		return tlrStitchTyp;
	}	
	public void setTlrStitchTyp(Set<TailorStitchType> tlrStitchTyp) {
		this.tlrStitchTyp = tlrStitchTyp;
	}
	

}
