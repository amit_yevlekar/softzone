package com.domain;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;


@Embeddable
public class TailorStitchId implements java.io.Serializable
{
	private Tailor tailor;
	private StitchType stitchType;
	
	@ManyToOne
	public Tailor getTailor() {
		return tailor;
	}
	public void setTailor(Tailor tailor) {
		this.tailor = tailor;
	}
	
	@ManyToOne
	public StitchType getStitchType() {
		return stitchType;
	}
	public void setStitchType(StitchType stitchType) {
		this.stitchType = stitchType;
	}
	
	
	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
 
        TailorStitchId that = (TailorStitchId) o;
 
        if (tailor != null ? !tailor.equals(that.tailor) : that.tailor != null) return false;
        if (stitchType != null ? !stitchType.equals(that.stitchType) : that.stitchType != null)
            return false;
 
        return true;
    }
 
    public int hashCode() {
        int result;
        result = (tailor != null ? tailor.hashCode() : 0);
        result = 31 * result + (stitchType != null ? stitchType.hashCode() : 0);
        return result;
    }

}
