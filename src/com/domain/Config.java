package com.domain;

public class Config {

	private String stitchId;
	private String stitchType;
	private String aster;
	private String romanChannel;
	private String smtpHost;
	private String smtpPort;
	private String smtpAuth;
	private String emailFrom;
	private String emailPwd;
	private String adminUser;
	private String adminPass;
	private String location;
	
	public Config(){}
	
	public Config(String smtpHost, String smtpPort, String smtpAuth){}	
	
	public String getStitchId() {
		return stitchId;
	}
	public void setStitchId(String stitchId) {
		this.stitchId = stitchId;
	}
	
	public String getStitchType() {
		return stitchType;
	}
	public void setStitchType(String stitchType) {
		this.stitchType = stitchType;
	}
	
	public String getAster() {
		return aster;
	}
	public void setAster(String aster) {
		this.aster = aster;
	}
	
	public String getRomanChannel() {
		return romanChannel;
	}
	public void setRomanChannel(String romanChannel) {
		this.romanChannel = romanChannel;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getSmtpAuth() {
		return smtpAuth;
	}
	public void setSmtpAuth(String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getEmailPwd() {
		return emailPwd;
	}
	public void setEmailPwd(String emailPwd) {
		this.emailPwd = emailPwd;
	}
	public String getAdminUser() {
		return adminUser;
	}
	public void setAdminUser(String adminUser) {
		this.adminUser = adminUser;
	}
	public String getAdminPass() {
		return adminPass;
	}
	public void setAdminPass(String adminPass) {
		this.adminPass = adminPass;
	}	
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocation() {
		return location;
	}
}
