package com.domain;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="ORDER_INFO")
public class OrderInfo
{
	public OrderInfo() {}
	public OrderInfo(String orderNo)
	{
		this.orderNo = orderNo;
	}
	
	private String orderNo;
	
	private String customerName;
	private Long contactNo;
	private String address;
	private Long tailorId;
	private String deliveryDate;
	private String status;
	private Date creationDate;
	private Date updateDate;
	private Double totalCharges;
	
	private Set<Window> windowLst = new TreeSet<Window>();
	
	private float discount;

	@Id
	@Column(name="ORDER_NO")
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="CUSTOMER_NAME")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name="CONTACT_NO")
	public Long getContactNo() {
		return contactNo;
	}

	public void setContactNo(Long contactNo) {
		this.contactNo = contactNo;
	}

	@Column(name="ADDRESS")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name="TAILOR_ID")
	public Long getTailorId() {
		return tailorId;
	}

	public void setTailorId(Long tailorId) {
		this.tailorId = tailorId;
	}
	
	@Column(name="DELIVERY_DATE")
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "orderInfo", cascade = CascadeType.ALL, orphanRemoval=true)
	@OrderBy("windowId")
	public Set<Window> getWindowLst() {
		return windowLst;
	}
	public void setWindowLst(Set<Window> windowLst) {
		this.windowLst = windowLst;
	}

	@Column(name="DISCOUNT")
	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}
	
	@Column(name="STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="CREATED", updatable=false)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="UPDATED")
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	@Column(name="TOT_CHRG")
	public Double getTotalCharges() {
		return totalCharges;
	}
	public void setTotalCharges(Double totalCharges) {
		this.totalCharges = totalCharges;
	}
	
	public void addToTotal(Double amt)
	{
		this.totalCharges += amt;
	}
	
}
